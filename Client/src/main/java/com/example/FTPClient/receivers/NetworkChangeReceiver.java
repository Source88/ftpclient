package com.example.FTPClient.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.Request;
import com.example.FTPClient.events.MessageEvent;
import com.example.FTPClient.managers.FTPManager;
import de.greenrobot.event.EventBus;

/**
 * Created by Sergey Shuruta on 4/16/14 at 2:44 PM.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {

    private FTPManager mFTPManager;

    public NetworkChangeReceiver() {
        mFTPManager = FTPApplication.getInstance().getFTPManager();
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        mFTPManager.updateConnectivityStatus();
        EventBus.getDefault().post(new MessageEvent(Request.EVENT_INTERNET_STATUS_CHANGED));
    }
}