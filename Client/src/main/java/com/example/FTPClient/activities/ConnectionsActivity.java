package com.example.FTPClient.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import com.example.FTPClient.Const;
import com.example.FTPClient.R;
import com.example.FTPClient.adapters.ConsListAdapter;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.database.DBConnections;
import com.example.FTPClient.gui.GUIConDialogHolder;

/**
 * Author: Sergey Shuruta
 * Date: 15.09.13
 * Time: 18:55
 */
public class ConnectionsActivity extends Activity implements AdapterView.OnItemClickListener,
                                                        View.OnClickListener {

    private ConsListAdapter mAdapter;
    private GUIConDialogHolder mGUI;
    private DBConnections mDataBase;
    private SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGUI = new GUIConDialogHolder(this, R.layout.dialog_connections_list);
        mSharedPreferences = FTPApplication.getInstance().getSharedPreferences();
        mDataBase = new DBConnections(ConnectionsActivity.this);
        mDataBase.open();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAdapter = new ConsListAdapter(ConnectionsActivity.this, mDataBase.getConnections());
        mGUI.consList.setAdapter(mAdapter);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mSharedPreferences.edit().putLong(Const.CONNECTION_SELECTED_ID, mAdapter.getItemId(position)).commit();
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case GUIConDialogHolder.ID_NEW_CON:
                Intent editConnection = new Intent(ConnectionsActivity.this, EditConnection.class);
                startActivity(editConnection);
                break;
            case GUIConDialogHolder.ID_CLOSE:
                finish();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDataBase.close();
    }
}
