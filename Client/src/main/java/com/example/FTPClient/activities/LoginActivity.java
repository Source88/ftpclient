package com.example.FTPClient.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.FTPClient.Const;
import com.example.FTPClient.R;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.database.DBConnections;
import com.example.FTPClient.entity.Connection;
import com.example.FTPClient.events.MessageEvent;
import com.example.FTPClient.managers.FTPManager;

import de.greenrobot.event.EventBus;

public class LoginActivity extends FragmentActivity implements OnClickListener {

    private FTPManager mFTPManager;
    private Holder mHolder;
    private DBConnections mDataBase;
    private SharedPreferences mSharedPreferences;
    private long mSelectConnectionId;
    private Connection mConnection;
    private EventBus mBus;

    public static final String TAG = LoginActivity.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHolder = new Holder(this, R.layout.activity_main);
        mFTPManager = FTPApplication.getInstance().getFTPManager();
        mSharedPreferences = FTPApplication.getInstance().getSharedPreferences();
        mDataBase = DBConnections.getDB(this);
        mBus = EventBus.getDefault();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBus.register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mBus.unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHolder.setInternetConnectionState(mFTPManager.getInternetStatus());
        mSelectConnectionId = mSharedPreferences.getLong(Const.CONNECTION_SELECTED_ID, 0);
        if(mSelectConnectionId > 0) {
            mConnection = mDataBase.getItemById(mSelectConnectionId);
            mHolder.setSelectConnection(mConnection.getName());
        } else {
            mConnection = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(R.string.settings)
                .setIcon(R.drawable.ic_settings)
                .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        startActivity(new Intent(LoginActivity.this, SettingsActivity.class));
                        return true;
                    }
                }).setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case Holder.ID_CONNECTION_SELECT:
                startActivity(new Intent(LoginActivity.this, ((mDataBase.getSize() > 0) ? ConnectionsActivity.class : EditConnection.class)));
                break;
            case Holder.ID_CONNECTION_START:
                if(mConnection == null) break;
                mFTPManager.connect(mConnection);
                break;
        }
    }

    public void onEventMainThread(MessageEvent event) {
        switch (event.getAction()) {
            case EVENT_INTERNET_STATUS_CHANGED:
                mHolder.setInternetConnectionState(mFTPManager.getInternetStatus());
                break;
            case REQUEST_CONNECTION_START:
                Log.d(TAG, "Connection start.");
                mHolder.startConnection();
                break;
            case REQUEST_CONNECTION_OK:
                Log.d(TAG, "Connection OK!");
                mHolder.startReadList();
                mFTPManager.readList();
                break;
            case REQUEST_AUTHORIZATION_ERROR:
                Log.d(TAG, "Authorization error!");
                mHolder.stopConnection(R.string.authorization_error);
                break;
            case REQUEST_CONNECTION_ERROR:
                Log.d(TAG, "Connection error!");
                mHolder.stopConnection(R.string.connection_error);
                break;
            case REQUEST_READ_LIST_FINISH:
                Log.d(TAG ,"Read list finish.");
                startActivity(new Intent(this, SessionActivity.class));
                mHolder.stopConnection();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDataBase.close();
    }

    private class Holder {

        private Activity mActivity;
        private TextView internetStatus, infoProgress;
        private Button selectConnection, doConnection;
        private LinearLayout progressPanel;

        public static final int ID_CONNECTION_SELECT = R.id.connect_select;
        public static final int ID_CONNECTION_START = R.id.connect_do;

        public Holder(Activity activity, int layout) {
            mActivity = activity;
            //mActivity.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            mActivity.setContentView(layout);
            initGUI();
        }

        private void initGUI() {
            internetStatus = (TextView) mActivity.findViewById(R.id.intenet_status);
            selectConnection = (Button) mActivity.findViewById(ID_CONNECTION_SELECT);
            doConnection = (Button) mActivity.findViewById(ID_CONNECTION_START);
            progressPanel = (LinearLayout) mActivity.findViewById(R.id.progress_panel);
            infoProgress = (TextView) mActivity.findViewById(R.id.info_progress);
            selectConnection.setOnClickListener((View.OnClickListener) mActivity);
            doConnection.setOnClickListener((View.OnClickListener) mActivity);
        }

        public void setInternetConnectionState(boolean isConnected) {
            doConnection.setEnabled(isConnected);
            internetStatus.setText(isConnected ? R.string.ok_intenet_connection : R.string.no_intenet_connection);
        }

        public void startConnection() {
            doConnection.setEnabled(false);
            selectConnection.setEnabled(false);
            infoProgress.setText(R.string.connecting_pt);
            progressPanel.setVisibility(View.VISIBLE);
        }

        public void startReadList() {
            infoProgress.setText(R.string.reading_list_files_pt);
        }

        public void stopConnection() {
            doConnection.setEnabled(true);
            selectConnection.setEnabled(true);
            progressPanel.setVisibility(View.INVISIBLE);
        }

        public void stopConnection(int res) {
            stopConnection();
            Toast.makeText(mActivity, mActivity.getString(res), Toast.LENGTH_LONG).show();
        }

        public void setSelectConnection(String name) {
            selectConnection.setText(name);
        }
    }
}