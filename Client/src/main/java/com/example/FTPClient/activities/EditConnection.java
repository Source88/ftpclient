package com.example.FTPClient.activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import com.example.FTPClient.Const;
import com.example.FTPClient.R;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.database.DBConnections;
import com.example.FTPClient.entity.Connection;
import com.example.FTPClient.gui.GUIEditHolder;

/**
 * Author: Sergey Shuruta
 * Date: 10/3/13
 * Time: 2:55 PM
 */
public class EditConnection extends Activity implements View.OnClickListener {

    private DBConnections mDataBase;
    private GUIEditHolder mGUI;
    private SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSharedPreferences = FTPApplication.getInstance().getSharedPreferences();
        mDataBase = DBConnections.getDB(this);
        mGUI = new GUIEditHolder(this, R.layout.activity_edit_connection);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case GUIEditHolder.ID_SAVE_CONNECTION:
                Connection connection = new Connection();
                connection.setName(mGUI.name.getText().toString());
                connection.setServer(mGUI.server.getText().toString());
                connection.setPort(mGUI.port.getText().toString());
                connection.setLogin(mGUI.login.getText().toString());
                connection.setPassword(mGUI.password.getText().toString());
                connection.setId(mDataBase.addRec(connection));
                mSharedPreferences.edit().putLong(Const.CONNECTION_SELECTED_ID, connection.getId()).commit();
                finish();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDataBase.close();
    }
}
