package com.example.FTPClient.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;

import com.example.FTPClient.R;
import com.example.FTPClient.Utils;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.dialogs.FilePresentDialog;
import com.example.FTPClient.dialogs.NewFolderDialog;
import com.example.FTPClient.dialogs.NoInternetDialog;
import com.example.FTPClient.dialogs.ReconnectDialog;
import com.example.FTPClient.events.MessageEvent;
import com.example.FTPClient.events.SelectListEvent;
import com.example.FTPClient.fragments.ActionMenuListener;
import com.example.FTPClient.fragments.BusyListener;
import com.example.FTPClient.fragments.FTPListFragment;
import com.example.FTPClient.fragments.LocalListFragment;
import com.example.FTPClient.managers.FTPManager;

import java.io.File;

import de.greenrobot.event.EventBus;

/**
 * Author: Sergey Shuruta
 * Date: 10/17/13
 * Time: 3:32 PM
 */
public class SessionActivity extends FragmentActivity implements BusyListener {

    private FTPManager mFTPManager;
    private FragmentTransaction mFragmentTransaction;
    private FTPListFragment mFTPListFragment;
    private LocalListFragment mLocalListFragment;
    private EventBus mBus;
    boolean isBusy = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);
        mBus = EventBus.getDefault();
        setContentView(R.layout.activity_session);
        addFragments();
        mFTPManager = FTPApplication.getInstance().getFTPManager();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.back:
                if(!isBusy) {
                    startLoading();
                    getCurrentListener().onBackAction();
                }
                break;
            case R.id.new_folder:
                if(!isBusy) {
                    /*DialogFactory.display(SessionActivity.this, new NewFolderDialog(getCurrentListener()), new DialogFactory.OnFinalizeListener() {
                        @Override
                        public void finalize(DialogFactory.DialogEvent event) {
                            getCurrentListener().onRefreshAction();
                        }
                    });*/
                    NewFolderDialog.create(SessionActivity.this, FTPApplication.getInstance().getSelectedPanel(), getSelectedPath());
                }
                break;
            case R.id.refresh:
                if(!isBusy) {
                    startLoading();
                    getCurrentListener().onRefreshAction();
                }
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBus.register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mBus.unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mBus.unregister(this);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SessionActivity.this);
        builder.setTitle(R.string.close_connection_ask);
        builder.setCancelable(false);
        builder.setMessage(String.format(getResources().getString(R.string.close_connection_dialog_ask), mFTPManager.getConnection().getName()))
                .setPositiveButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.close_connection, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mFTPManager.setCurrentDir(File.separator);
                        mFTPManager.disconnect(false);
                    }
                });
        builder.show();
    }

    private ActionMenuListener getCurrentListener() {
        if(isThisSelected(FTPApplication.ListType.FTP))
            return mFTPListFragment.getActionMenuListener();
        else
            return mLocalListFragment.getActionMenuListener();
    }

    private boolean isThisSelected(FTPApplication.ListType type) {
        return FTPApplication.getInstance().getSelectedPanel() == type;
    }

    public void onEventMainThread(MessageEvent event) {
        switch (event.getAction()) {
            case REQUEST_CONNECTION_OK:
                mFTPManager.readList();
                break;
            case REQUEST_DISCONNECT_OK:
                FTPManager.Reconnect value = (FTPManager.Reconnect) event.getValue();
                if(value == FTPManager.Reconnect.YES) {
                    mFTPManager.connect();
                } else if (value == FTPManager.Reconnect.NO_AND_CLOSE) {
                    finish();
                }
                break;
            case REQUEST_DISCONNECT_ERROR:
                finish();
                break;
            case REQUEST_DOWNLOAD_FILE_PRESENT:
                FilePresentDialog.create(this).show();
                break;
            case REQUEST_DOWNLOAD_POOL_ERROR:
/*                if(mFTPManager.updateConnectivityStatus()) {
                    ReconnectDialog.create(this).show();
                } else {
                    mFTPManager.disconnect(false, FTPManager.Reconnect.NO);
                    NoInternetDialog.create(this).show();
                }*/
                break;
            case REQUEST_READ_LIST_FINISH:
                stopLoading();
                break;
            case REQUEST_READ_LIST_ERROR:
                stopLoading();
                if(mFTPManager.updateConnectivityStatus()) {
                    ReconnectDialog.create(this).show();
                } else {
                    mFTPManager.disconnect(false, FTPManager.Reconnect.NO);
                    NoInternetDialog.create(this).show();
                }
                break;
        }
    }

    @Override
    public void startLoading() {
        if(!isThisSelected(FTPApplication.ListType.FTP)) return;
        isBusy = true;
        setProgressBarIndeterminateVisibility(true);
        mFTPListFragment.setLoading(true);
    }

    @Override
    public void stopLoading() {
        if(!isThisSelected(FTPApplication.ListType.FTP)) return;
        isBusy = false;
        setProgressBarIndeterminateVisibility(false);
        mFTPListFragment.setLoading(false);
    }

    @Override
    public boolean isBusy() {
        return isBusy;
    }

    private void addFragments() {
        mFragmentTransaction = getSupportFragmentManager().beginTransaction();
        mFTPListFragment = new FTPListFragment();
        mLocalListFragment = new LocalListFragment();
        mFragmentTransaction.add(R.id.ftp_frame, mFTPListFragment);
        if(Utils.isLandscapeOrientation()) {
            mFragmentTransaction.add(R.id.local_frame, mLocalListFragment);
        }

        ((FrameLayout)findViewById(R.id.ftp_frame)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new SelectListEvent(FTPApplication.ListType.FTP));
            }
        });

        FrameLayout localFrame = (FrameLayout)findViewById(R.id.local_frame);
        if(localFrame != null)
            localFrame.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new SelectListEvent(FTPApplication.ListType.LOCAL));
                }
            });

        mFragmentTransaction.addToBackStack(null);
        mFragmentTransaction.commit();
    }

    private File getSelectedPath() {
        if(isThisSelected(FTPApplication.ListType.FTP))
            return mFTPListFragment.getPath();
        else
            return mLocalListFragment.getPath();
    }
}
