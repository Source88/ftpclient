package com.example.FTPClient.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;

import com.example.FTPClient.R;
import com.example.FTPClient.Request;
import com.example.FTPClient.Utils;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.events.MessageEvent;
import com.example.FTPClient.fragments.BusyListener;
import com.example.FTPClient.fragments.LocalListFragment;
import com.example.FTPClient.managers.FTPManager;

import de.greenrobot.event.EventBus;

/**
 * Author: Sergey Shuruta
 * Date: 10/17/13
 * Time: 3:32 PM
 */
public class LocalActivity extends FragmentActivity implements BusyListener {

    private Holder mHolder;
    private FragmentTransaction mFragmentTransaction;
    private LocalListFragment mLocalListFragment;

    public static final String TAG = LocalActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_local);
        FTPApplication.getInstance().setSelectedPanel(FTPApplication.ListType.LOCAL);
        final FTPManager mFTPManager = FTPApplication.getInstance().getFTPManager();
        mHolder = new Holder();

        mFragmentTransaction = getSupportFragmentManager().beginTransaction();
        mLocalListFragment = new LocalListFragment(true);
        mFragmentTransaction.add(R.id.local_frame, mLocalListFragment);
        mFragmentTransaction.commit();

        mHolder.selectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessageEvent event = new MessageEvent(Request.REQUEST_DOWNLOAD_START);
                event.setValue(mLocalListFragment.getPath());
                mFTPManager.getConnection().setCurrentDir(event.getValue().toString());
                EventBus.getDefault().post(event);
                finish();
            }
        });

        mHolder.cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(Utils.isLandscapeOrientation()) {
            finish();
        }
    }

    @Override
    public void startLoading() {
        return;
    }

    @Override
    public void stopLoading() {
        return;
    }

    @Override
    public boolean isBusy() {
        return false;
    }

    private class Holder {
        public Button selectBtn;
        public Button cancelBtn;

        private Holder() {
            selectBtn = (Button) findViewById(R.id.select);
            cancelBtn = (Button) findViewById(R.id.cancel_btn);
        }
    }
}
