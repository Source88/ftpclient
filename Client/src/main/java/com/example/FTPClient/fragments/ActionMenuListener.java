package com.example.FTPClient.fragments;

/**
 * Created by Sergey Shuruta on 6/17/14 at 12:27 PM.
 */
public interface ActionMenuListener {
    public void onBackAction();
    public void onRefreshAction();
}
