package com.example.FTPClient.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.example.FTPClient.Const;
import com.example.FTPClient.entity.FileUnit;
import com.example.FTPClient.Utils;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.dialogs.FileInfoDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Stack;

/**
 * Author: Sergey Shuruta
 * Date: 10/17/13
 * Time: 3:56 PM
 */
public class LocalListFragment extends FilesListFragment {

    private Stack<String> mLocalDir = new Stack<String>();
    private SharedPreferences mSharedPreferences;

    public static final String TAG = LocalListFragment.class.getSimpleName();

    public LocalListFragment() {}

    public LocalListFragment(boolean isDialog) {
        if(isDialog)
            mWithControls = true;
    }

    @Override
    public void onCreateList(Bundle savedInstanceState) {
        setListType(FTPApplication.ListType.LOCAL);
        setFileMenu(mFileMenuListener);
        setActionMenu(mActionMenuListener);
        mSharedPreferences = FTPApplication.getInstance().getSharedPreferences();
        String patch = mSharedPreferences.getString(Const.SHARED_CURRENT_LOCAL_DIR, FTPApplication.getInstance().getDownloadFolder());
        mLocalDir.addAll(Utils.getListFromStr(patch, File.separator));
    }

    private FileMenuListener mFileMenuListener = new FileMenuListener() {

        @Override
        public void onDownload(List<FileUnit> files) {

        }

        @Override
        public void onUpload(List<FileUnit> files) {

        }

        @Override
        public void onShowProperties(FileUnit file) {
            FileInfoDialog.create(getActivity(), file).show();
        }

        @Override
        public FTPApplication.ListType getListType() {
            return FTPApplication.ListType.LOCAL;
        }
    };


    private ActionMenuListener mActionMenuListener = new ActionMenuListener() {

        @Override
        public void onBackAction() {
            mLocalDir.pop();
            onRefreshList();
            mSharedPreferences.edit().putString(Const.SHARED_CURRENT_LOCAL_DIR, getStringPath()).commit();
        }

        @Override
        public void onRefreshAction() {
            onRefreshList();
        }
    };

    @Override
    public void onRefreshList() {
        readFiles();
    }

    public void readFiles() {
        List<FileUnit> files = new ArrayList<FileUnit>();
        File dir = new File(getStringPath());
        for (File file : dir.listFiles()) {
            if(!file.isHidden()) {
                // TODO add in settings_headers checkbox for display or not hidden files
                files.add(new FileUnit(file));
            }
        }
        Collections.sort(files, new Comparator<FileUnit>() {
            @Override
            public int compare(final FileUnit object1, final FileUnit object2) {
                if (object1.isDir() && object2.isFile()) return -1;
                if (object1.isFile() && object2.isDir()) return 1;
                return object1.getName().compareTo(object2.getName());
            }
        });
        setFiles(files);
    }

    public String getStringPath() {
        String path = File.separator;
        for(String dir : mLocalDir) {
            if(!dir.isEmpty()) {
                path = path.concat(dir).concat(File.separator);
            }
        }
        return path;
    }

    @Override
    public File getPath() {
        return new File(getStringPath());
    }

    @Override
    void onItemClickList(String dir) {
        mLocalDir.push(dir);
        readFiles();
        mSharedPreferences.edit().putString(Const.SHARED_CURRENT_LOCAL_DIR, getStringPath()).commit();
    }
}
