package com.example.FTPClient.fragments;

/**
 * Created by Source on 06.06.2014.
 */
public interface PopupInterface {

    void onDownload();
    void onDownloadTo();
    void onUpload();
    void onUploadTo();
    void onDelete();
    void onRename();
    void onShowProperties();
}
