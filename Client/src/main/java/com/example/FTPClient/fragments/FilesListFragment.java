package com.example.FTPClient.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.FTPClient.dialogs.DownloadToDialog;
import com.example.FTPClient.entity.FileUnit;
import com.example.FTPClient.R;
import com.example.FTPClient.Utils;
import com.example.FTPClient.activities.LocalActivity;
import com.example.FTPClient.adapters.FileAdapter;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.dialogs.DeleteDialog;
import com.example.FTPClient.dialogs.DownloadDialog2;
import com.example.FTPClient.dialogs.NewFolderDialog;
import com.example.FTPClient.dialogs.RenameDialog;
import com.example.FTPClient.entity.HeapFiles;
import com.example.FTPClient.events.MessageEvent;
import com.example.FTPClient.events.SelectListEvent;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by Sergey Shuruta on 5/6/14 at 10:59 AM.
 */
public abstract class FilesListFragment extends Fragment implements
        AdapterView.OnItemClickListener,
        View.OnTouchListener {

    private ListView mListView;
    private TextView mPath;
    private FileAdapter mAdapter;
    private ActionMode mActionMode;
    private List<FileUnit> mFiles = new ArrayList<FileUnit>();
    private boolean isBusy = false;
    private FTPApplication.ListType mListType;
    private FileMenuListener mFileMenuListener;
    private ActionMenuListener mActionMenuListener;
    private boolean mTouchSelected = false;
    protected boolean mWithControls = false;
    private ImageButton mDeleteBtn;

    private BusyListener mBusyListener;

    abstract public void onCreateList(Bundle savedInstanceState);
    abstract public void onRefreshList();

    abstract void onItemClickList(String dir);
    abstract public File getPath();

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mBusyListener = (BusyListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement BusyListener");
        }
    }

    @Override
    public final void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        onRefreshList();
        displaySelectedPanel(FTPApplication.getInstance().getSelectedPanel());
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onCreateList(savedInstanceState);
        if(mListType == null || mFileMenuListener == null || mActionMenuListener == null) {
            throw new ClassCastException("Class inherits from " + FilesListFragment.class.getSimpleName() + " must setFileMenu(<FileMenuListener>) and setActionMenu(<ActionMenuListener>)");
        }
        mAdapter = new FileAdapter(getActivity(), mFiles, mSelectListener, mPopupListener);
    }

    public void onEvent(SelectListEvent event) {
        if(event.getListType() == mListType)
            FTPApplication.getInstance().setSelectedPanel(mListType);
        displaySelectedPanel(event.getListType());
    }

    protected void setListType(FTPApplication.ListType listType) {
        mListType = listType;
    }

    protected void setFileMenu(FileMenuListener listener) {
        mFileMenuListener = listener;
    }

    protected void setActionMenu(ActionMenuListener listener) {
        mActionMenuListener = listener;
    }


    public ActionMenuListener getActionMenuListener() {
        return mActionMenuListener;
    }

    public void setLoading(boolean isLoading) {
        if(!isLoading) {
            mListView.setEnabled(true);
            mListView.setAlpha(1.0f);
        } else {
            mListView.setEnabled(false);
            mListView.setAlpha(0.8f);

        }
    }

    protected void setFiles(List<FileUnit> files) {
        mAdapter.clearSelection();
        mFiles.clear();
        mFiles.addAll(files);
        mAdapter.notifyDataSetChanged();
        mPath.setText(getString(mListType == FTPApplication.ListType.FTP ? R.string.ftp_list : R.string.local_list) + getPath().getPath());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_file_list, null);
        mPath = (TextView) view.findViewById(R.id.path);
        mListView = (ListView) view.findViewById(R.id.file_list);
        mListView.setOnItemClickListener(this);
        mListView.setOnTouchListener(this);
        mListView.setEmptyView((LinearLayout) view.findViewById(R.id.info_panel));

        if(mWithControls) {
            ((RelativeLayout) view.findViewById(R.id.root)).setVisibility(View.VISIBLE);

            mDeleteBtn = (ImageButton) view.findViewById(R.id.delete);
            mDeleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    DeleteDialog.create(getActivity(), mListType, mAdapter.getSelected());
                }
            });

            ((ImageButton) view.findViewById(R.id.back)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!mBusyListener.isBusy()) {
                        mBusyListener.startLoading();
                        mActionMenuListener.onBackAction();
                    }
                }
            });

            ((ImageButton) view.findViewById(R.id.new_dir)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!isBusy) {
                        NewFolderDialog.create(getActivity(), mListType, getPath());
                    }
                }
            });

            ((ImageButton) view.findViewById(R.id.refresh)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    refreshList();
                }
            });
        }
        mListView.setAdapter(mAdapter);
        mPath.setText(getString(mListType == FTPApplication.ListType.FTP ? R.string.ftp_list : R.string.local_list) + getPath().getPath());
        return view;
    }

    private void displaySelectedPanel(FTPApplication.ListType ListType) {
        if(Utils.isLandscapeOrientation()) {
            if(mActionMode != null)
                mActionMode.finish();
            mPath.setBackgroundResource(ListType == mListType ? R.drawable.path_panel_bg_active : R.drawable.path_panel_bg);
        }
    }

    private View.OnClickListener mPopupListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            if (FTPApplication.getInstance().getSelectedPanel() != mListType) {
                EventBus.getDefault().post(new SelectListEvent(mListType));
                return;
            }

            if(mActionMode != null)
                mActionMode.finish();
            mAdapter.clearSelection();
            mAdapter.select(view);
            PopupMenu popup = new PopupMenu(getActivity(), view);
            MenuInflater inflater = popup.getMenuInflater();
            inflater.inflate(R.menu.menu_file, popup.getMenu());
            if(mWithControls) {
                popup.getMenu().findItem(R.id.load).setVisible(false);
                popup.getMenu().findItem(R.id.properties).setVisible(false);
            }
            popup.setOnDismissListener(new PopupMenu.OnDismissListener() {
                @Override
                public void onDismiss(PopupMenu menu) {
                    mAdapter.clearSelection();
                    mAdapter.notifyDataSetChanged();
                }
            });
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    return onFileMenuItemClick(item.getItemId());
                }
            });
            if(mListType == FTPApplication.ListType.FTP) {
                popup.getMenu().findItem(R.id.load).setTitle(R.string.download);
                popup.getMenu().findItem(R.id.load).setIcon(getActivity().getResources().getDrawable(R.drawable.ic_download));
            } else {
                popup.getMenu().findItem(R.id.load).setTitle(R.string.upload);
                popup.getMenu().findItem(R.id.load).setIcon(getActivity().getResources().getDrawable(R.drawable.ic_upload));
            }
            popup.show();
        }
    };

    private boolean onFileMenuItemClick(int itemId) {
        switch (itemId) {
            case R.id.load:
                if(mListType == FTPApplication.ListType.FTP) {
                    if(Utils.isLandscapeOrientation()) {
                        FTPApplication.getInstance().getFTPManager().addToDownload(new HeapFiles(mAdapter.getSelected()));
                        DownloadToDialog.create(getActivity(), mListType);
                    } else {
                        Intent intent = new Intent(getActivity(), LocalActivity.class);
                        startActivity(intent);
                    }
                } else {
                    DownloadToDialog.create(getActivity(), mListType);
                }
                return true;
            case R.id.rename:
                RenameDialog.create(getActivity(), mListType, mAdapter.getSelected(0));
                return true;
            case R.id.delete:
                DeleteDialog.create(getActivity(), mListType, mAdapter.getSelected());
                return true;
            case R.id.properties:
                //mFileMenuListener.onShowProperties(mAdapter.getSelected(0));
                return true;
            default:
                return false;
        }
    }

    private View.OnClickListener mSelectListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            if (FTPApplication.getInstance().getSelectedPanel() != mListType) {
                EventBus.getDefault().post(new SelectListEvent(mListType));
                return;
            }

            mAdapter.select(view);

            int selectedCount = mAdapter.getSelectedCount();
            if(!mWithControls) {
                if (mActionMode == null) {
                    mActionMode = getActivity().startActionMode(new FTPActionModes());
                } else if (selectedCount == 0) {
                    mActionMode.finish();
                    mActionMode = null;
                } else if(selectedCount == 1) {
                    mActionMode.getMenu().findItem(R.id.rename).setVisible(true);
                    mActionMode.getMenu().findItem(R.id.properties).setVisible(true);
                } else {
                    mActionMode.getMenu().findItem(R.id.rename).setVisible(false);
                    mActionMode.getMenu().findItem(R.id.properties).setVisible(false);
                }
                if(mActionMode != null) {
                    mActionMode.setTitle(getString(R.string.selected_var_files, selectedCount));
                }
            } else {
                if(selectedCount > 0)
                    mDeleteBtn.setVisibility(View.VISIBLE);
                else
                    mDeleteBtn.setVisibility(View.GONE);
            }
            mAdapter.notifyDataSetChanged();
        }
    };

    private void refreshList() {
        if(!mBusyListener.isBusy()) {
            if(mListType == FTPApplication.ListType.FTP) {
                mBusyListener.startLoading();
            }
            mActionMenuListener.onRefreshAction();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if(mTouchSelected) {
            mTouchSelected = false;
            return;
        }

        if(mAdapter.getCount() <= position) return;

        FileUnit file = mAdapter.getItem(position);
        if (file.isFile()) return;
        mAdapter.clearSelection();
        onItemClickList(file.getName());
        if (mBusyListener != null)
            mBusyListener.startLoading();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (FTPApplication.getInstance().getSelectedPanel() != mListType) {
            mTouchSelected = true;
            EventBus.getDefault().post(new SelectListEvent(mListType));
            return true;
        }
        return false;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(onFileMenuItemClick(item.getItemId()))
            return true;
        else return super.onContextItemSelected(item);
    }

    public class FTPActionModes implements ActionMode.Callback {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mActionMode = mode;
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.menu_file, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            boolean result = onFileMenuItemClick(item.getItemId());
            // ToDo execute after deleting
            mActionMode.finish();
            return result;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mAdapter.clearSelection();
            mAdapter.notifyDataSetChanged();
            mActionMode = null;
        }
    }

    public void onEventMainThread(MessageEvent event) {
        switch (event.getAction()) {
            case EVENT_REFRESH_LIST:
                refreshList();
                break;
            case REQUEST_DOWNLOAD_START:
                //DownloadToDialog.create(getActivity(), mListType);
                break;
            case REQUEST_DOWNLOAD_POOL_START:
                Log.d("TEST", "REQUEST_DOWNLOAD_POOL_START");
                DownloadDialog2.create(getActivity(), mListType);
                break;
        }
    }
}
