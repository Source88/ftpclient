package com.example.FTPClient.fragments;

import android.os.Bundle;
import android.widget.Toast;

import com.example.FTPClient.dialogs.DownloadDialog2;
import com.example.FTPClient.entity.FileUnit;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.dialogs.FileInfoDialog;
import com.example.FTPClient.events.MessageEvent;
import com.example.FTPClient.managers.FTPManager;

import java.io.File;
import java.util.List;

/**
 * Author: Sergey Shuruta
 * Date: 10/17/13
 * Time: 3:56 PM
 */
public class FTPListFragment extends FilesListFragment {

    private FTPManager mFTPManager;

    public static final String TAG = FTPListFragment.class.getSimpleName();

    @Override
    public void onCreateList(Bundle savedInstanceState) {
        setListType(FTPApplication.ListType.FTP);
        setFileMenu(mFileMenuListener);
        setActionMenu(mActionMenuListener);
        mFTPManager = FTPApplication.getInstance().getFTPManager();
}

    @Override
    public void onRefreshList() {
        setFiles(FTPApplication.getInstance().getFTPManager().getListFiles());
    }

    @Override
    public void onItemClickList(String dir) {
        mFTPManager.setCurrentDir(dir);
        mFTPManager.readList();
    }

    private FileMenuListener mFileMenuListener = new FileMenuListener() {

        @Override
        public void onDownload(List<FileUnit> files) {
/*            mFTPManager.startDownload(new HeapFiles(files));
            Intent intent = new Intent(getActivity(), LocalActivity.class);
            startActivity(intent);*/

            //mFTPManager.startDownloadTo(getActivity(), new BunchFiles(files));
        }

        @Override
        public void onUpload(List<FileUnit> files) {

        }

        @Override
        public void onShowProperties(FileUnit file) {
            FileInfoDialog.create(getActivity(), file).show();
        }

        @Override
        public FTPApplication.ListType getListType() {
            return FTPApplication.ListType.FTP;
        }
    };

    private ActionMenuListener mActionMenuListener = new ActionMenuListener() {

        @Override
        public void onBackAction() {
            mFTPManager.backCurrentDir();
            mFTPManager.readList();
        }

        @Override
        public void onRefreshAction() {
            mFTPManager.readList();
        }

/*        @Override
        public void onNewFolderAction(String name) {
            //mFTPManager.createFTPFolder(name);
        }*/

    };

    @Override
    public File getPath() {
        return new File((mFTPManager.getFTPCurrentDir().length() == 0) ? File.separator : mFTPManager.getFTPCurrentDir());
    }

    @Override
    public void onEventMainThread(MessageEvent event) {
        super.onEventMainThread(event);
        switch (event.getAction()) {
            case REQUEST_CONNECTION_OK:
                mFTPManager.readList();
                break;
/*            case REQUEST_DOWNLOAD_START:
                mFTPManager.startDownload();
                break;*/
            case REQUEST_DOWNLOAD_POOL_START:
                //DownloadDialog2.create(getActivity(), FTPApplication.ListType.FTP);
                break;
            case REQUEST_READ_LIST_FINISH:
                onRefreshList();
                break;
            /*case REQUEST_FOLDER_CREATED_FINISH:
                mFTPManager.readList();
                break;
            case REQUEST_FOLDER_CREATED_FILED:
                Toast.makeText(getActivity(), "Error created folder!", Toast.LENGTH_SHORT).show();
                break;*/
            /*case REQUEST_REMOVE_FILES_START:
                RefreshDialog.create(getActivity()).show(R.string.deleting_files_pts);
                break;*/
            /*case REQUEST_REMOVE_FILES_FINISH:
                mFTPManager.readList();
                break;
            case REQUEST_REMOVE_FILES_FILED:
                Toast.makeText(getActivity(), "Error remove files!", Toast.LENGTH_SHORT).show();
                break;*/
            case REQUEST_RENAME_FILE_FINISH:
                mFTPManager.readList();
                break;
            case REQUEST_RENAME_FILE_FILED:
                Toast.makeText(getActivity(), "Error rename file!", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
