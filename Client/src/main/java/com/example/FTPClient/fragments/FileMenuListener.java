package com.example.FTPClient.fragments;

import com.example.FTPClient.entity.FileUnit;
import com.example.FTPClient.applications.FTPApplication;

import java.util.List;

/**
 * Created by Source on 06.06.2014.
 */
public interface FileMenuListener {

    public void onDownload(List<FileUnit> file);
    public void onUpload(List<FileUnit> file);
    public void onShowProperties(FileUnit file);
    public FTPApplication.ListType getListType();
}
