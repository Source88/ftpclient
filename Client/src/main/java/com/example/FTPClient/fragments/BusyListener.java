package com.example.FTPClient.fragments;

/**
 * Created by Sergey Shuruta on 6/17/14 at 4:13 PM.
 */
public interface BusyListener {
    public void startLoading();
    public void stopLoading();
    public boolean isBusy();
}
