package com.example.FTPClient.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.FTPClient.entity.Connection;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Source
 * Date: 15.09.13
 * Time: 20:11
 * To change this template use File | Settings | File Templates.
 */
public class DBConnections {

    public static final String DB_NAME = "ftpclient_connections_db";
    public static final int DB_VERSION = 1;
    public static final String DB_TABLE = "connections";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_SERVER = "server";
    public static final String COLUMN_PORT = "port";
    public static final String COLUMN_LOGIN = "login";
    public static final String COLUMN_PASSW = "passw";

    public static final String DB_CREATE =
            "create table " + DB_TABLE + "(" +
                    COLUMN_ID + " integer primary key autoincrement, " +
                    COLUMN_NAME + " text, " +
                    COLUMN_SERVER + " text," +
                    COLUMN_PORT + " integer," +
                    COLUMN_LOGIN + " text," +
                    COLUMN_PASSW + " text" +
                    ");";

    private final Context mContext;


    private DataBaseHelper mDBHelper;
    private SQLiteDatabase mDB;

    public static DBConnections getDB(Context context) {
        DBConnections db = new DBConnections(context);
        db.open();
        return db;
    }

    public DBConnections(Context context) {
        mContext = context;
    }

    // открыть подключение
    public void open() {
        mDBHelper = new DataBaseHelper(mContext, DB_NAME, null, DB_VERSION);
        mDB = mDBHelper.getWritableDatabase();
    }

    // закрыть подключение
    public void close() {
        if (mDB != null) mDB.close();
        if (mDBHelper != null) mDBHelper.close();
    }

    // получить все данные из таблицы DB_TABLE
    public List<Connection> getConnections() {
        return convertData(mDB.query(DB_TABLE, null, null, null, null, null, null));
    }

    // добавить запись в DB_TABLE
    public long addRec(Connection connection) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_NAME, connection.getName());
        cv.put(COLUMN_SERVER, connection.getServer());
        cv.put(COLUMN_PORT, connection.getPort());
        cv.put(COLUMN_LOGIN, connection.getLogin());
        cv.put(COLUMN_PASSW, connection.getPassword());
        return mDB.insert(DB_TABLE, null, cv);
    }

    //Доступ к menu_ftp_options argest.txt
    /*
    gphoto.menu_ftp_options.ukraine.com.ua
    gphoto_ftp / gh123454
    */

    // удалить запись из DB_TABLE
    public void delRec(long id) {
        mDB.delete(DB_TABLE, COLUMN_ID + " = " + id, null);
    }

    public int getSize() {
        Cursor cursor = mDB.query(DB_TABLE, null, null, null, null, null, null);
        int size = cursor.getCount();
        cursor.close();
        return size;
    }

    public Connection getItemById(long id) {
        Cursor cursor = mDB.rawQuery("SELECT * FROM " + DB_TABLE  + " where " + COLUMN_ID + " = " + id, null);
        List<Connection> connections = convertData(cursor);
        cursor.close();
        return (connections.size() > 0) ? connections.get(0) : new Connection();
    }

    private List<Connection> convertData(Cursor cursor) {
        List<Connection> connections = new ArrayList<Connection>();
        if(cursor == null || cursor.getCount() == 0) return connections;
        if(cursor == null) return connections;
        cursor.moveToFirst();
        do {
            Connection connection = new Connection();
            connection.setId(Long.valueOf(cursor.getString(cursor.getColumnIndex(DBConnections.COLUMN_ID))));
            connection.setName(cursor.getString(cursor.getColumnIndex(DBConnections.COLUMN_NAME)));
            connection.setServer(cursor.getString(cursor.getColumnIndex(DBConnections.COLUMN_SERVER)));
            connection.setPort(cursor.getInt(cursor.getColumnIndex(DBConnections.COLUMN_PORT)));
            connection.setLogin(cursor.getString(cursor.getColumnIndex(DBConnections.COLUMN_LOGIN)));
            connection.setPassword(cursor.getString(cursor.getColumnIndex(DBConnections.COLUMN_PASSW)));
            connections.add(connection);
        } while (cursor.moveToNext());
        return connections;
    }
}
