package com.example.FTPClient.dialogs;

import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;

import com.example.FTPClient.entity.FileUnit;
import com.example.FTPClient.R;
import com.example.FTPClient.Request;
import com.example.FTPClient.applications.FTPApplication;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sergey.shuruta on 8.07.2014.
 */
public class DeleteDialog extends FTPDialog {

    private static FTPApplication.ListType mListType;
    private static List<FileUnit> mFiles;

    public static final DeleteDialog create(FragmentActivity activity, FTPApplication.ListType listType, List<FileUnit> files) {
        DeleteDialog dialog = new DeleteDialog();
        dialog.mListType = listType;
        dialog.mFiles = files;
        dialog.show(activity.getSupportFragmentManager(), dialog.getTitle());
        return dialog;
    }

    public DeleteDialog() {}

    @Override
    protected View onCreateView() {
        return getActivity().getLayoutInflater().inflate(R.layout.dialog_delete, null);
    }

    @Override
    protected String getTitle() {
        if(!isAdded()) return null;
        if(mFiles.size() == 1) {
            return getString(R.string.do_you_want_delete_file_ask, mFiles.get(0).getName());
        } else {
            return getString(R.string.do_you_want_delete_files_ask, mFiles.size());
        }
    }

    @Override
    protected int getPositiveBtnName() {
        return R.string.delete;
    }

    @Override
    protected FTPApplication.ListType getListType() {
        return mListType;
    }

    @Override
    protected void onClickDoFTP() {
        FTPApplication.getInstance().getFTPManager().deleteFiles(mFiles);
        Log.d("TEST", "Trying to delete " + mFiles.get(0).getName());
    }

    @Override
    protected void onClickDoLocal() {
        for(FileUnit node : mFiles) {
            File file = new File(node.getLocalDir());
            if(node.isFile())
                try {
                    FileUtils.forceDelete(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            else
                try {
                    FileUtils.deleteDirectory(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    @Override
    protected List<Request> getRequestsError() {
        List<Request> result = new ArrayList<>();
        result.add(Request.REQUEST_REMOVE_FILES_FILED);
        return result;
    }

    @Override
    protected List<Request> getRequestsFinish() {
        List<Request> result = new ArrayList<>();
        result.add(Request.REQUEST_REMOVE_FILES_FINISH);
        return result;
    }

    @Override
    protected String getErrorMessage() {
        return mFiles.size() == 1
                ? getString(R.string.error_delete_file, mFiles.get(0).getName())
                : getString(R.string.error_delete_files, mFiles.size());
    }
}
