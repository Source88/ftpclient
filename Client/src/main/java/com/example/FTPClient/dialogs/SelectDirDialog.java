package com.example.FTPClient.dialogs;

import android.content.res.Resources;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.RelativeLayout;
import com.example.FTPClient.R;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.fragments.ActionMenuListener;
import com.example.FTPClient.fragments.LocalListFragment;
import com.example.FTPClient.managers.FTPManager;

/**
 * Created by Sergey Shuruta on 6/26/14 at 5:28 PM.
 */
public class SelectDirDialog implements DialogPattern {

    private ActionMenuListener mListener;

    public SelectDirDialog() {}

    public SelectDirDialog(ActionMenuListener listener) {
        mListener = listener;
    }

    @Override
    public void onCreate(FragmentActivity activity) {
        FTPApplication.getInstance().setSelectedPanel(FTPApplication.ListType.LOCAL);
        FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
        LocalListFragment mLocalListFragment = new LocalListFragment(true);
        fragmentTransaction.add(R.id.local_frame, mLocalListFragment);
        fragmentTransaction.commit();
    }

    @Override
    public String getTitle(Resources resources) {
        return "rerer";
    }

    @Override
    public FTPApplication.ListType getListType() {
        return null;
    }

    @Override
    public boolean isCancelable() {
        return false;
    }

    @Override
    public ButtonSettings getPositiveBtn() {
        return null;
    }

    @Override
    public ButtonSettings getNegativeBtn() {
        return null;
    }

    @Override
    public View getView(FragmentActivity activity) {
        RelativeLayout view = (RelativeLayout) activity.getLayoutInflater().inflate(R.layout.dialog_select_dir, null);


        FTPApplication.getInstance().setSelectedPanel(FTPApplication.ListType.LOCAL);
        final FTPManager mFTPManager = FTPApplication.getInstance().getFTPManager();
        //mHolder = new Holder();

/*
        mHolder.selectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
*/
/*                MessageEvent event = new MessageEvent(Request.REQUEST_DOWNLOAD_START);
                event.setValue(mLocalListFragment.getPath());
                mFTPManager.getConnection().setCurrentDir(event.getValue().toString());
                EventBus.getDefault().post(event);*//*

                mFTPManager.startDownload(mLocalListFragment.getStringPath());
                finish();
            }
        });

        mHolder.cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
*/

        return view;
    }

    @Override
    public boolean isListType(FTPApplication.ListType listType) {
        return false;
    }
}
