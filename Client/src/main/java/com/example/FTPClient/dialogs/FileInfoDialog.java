package com.example.FTPClient.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.webkit.MimeTypeMap;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.FTPClient.entity.FileUnit;
import com.example.FTPClient.R;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.managers.FTPManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergey Shuruta on 1/31/14.
 */
public class FileInfoDialog extends AlertDialog.Builder {

    private FTPManager mFTPManager;
    private Activity mActivity;
    private FtpFileInfo mFileInfo;

    public static FileInfoDialog create(Activity activity, FileUnit ftpFile) {
        return new FileInfoDialog(activity, ftpFile);
    }

    protected FileInfoDialog(Activity activity, FileUnit ftpFile) {
        super(activity);
        mActivity = activity;
        mFTPManager = FTPApplication.getInstance().getFTPManager();
        setTitle(null);
        FtpFileInfo fileInfo = getFileInfo(ftpFile);
        LinearLayout view = (LinearLayout) mActivity.getLayoutInflater().inflate(R.layout.dialog_file_info, null);
        ((TextView) view.findViewById(R.id.file_name)).setText(fileInfo.name);
        ((TextView) view.findViewById(R.id.file_type)).setText(fileInfo.type);
        ((TextView) view.findViewById(R.id.file_size)).setText(fileInfo.size);
        setView(view);
        setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    private FtpFileInfo getFileInfo(FileUnit ftpFile) {
        mFileInfo = new FtpFileInfo();

        mFileInfo.name = ftpFile.getName();
        mFileInfo.size = ftpFile.getSize();
        if(ftpFile.isFile()) {
            String extension = mFileInfo.name.substring(mFileInfo.name.indexOf(".") + 1);
            String type = null;
            if (extension != null) {
                MimeTypeMap mime = MimeTypeMap.getSingleton();
                type = mime.getMimeTypeFromExtension(extension);
            }
            mFileInfo.type = type == null ? ".".concat(extension) : type.concat(" (.").concat(extension).concat(")");
        } else {
            mFileInfo.type = getContext().getString(R.string.directory);
        }
        final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);

        final List<ResolveInfo> pkgAppsList = mActivity.getPackageManager().queryIntentActivities(mainIntent, 0);
        for(int index = 0; index < pkgAppsList.size(); index++) {
            try {
                ApplicationInfo app = mActivity.getPackageManager().getApplicationInfo(pkgAppsList.get(index).activityInfo.packageName, 0);
                mFileInfo.packages.add(pkgAppsList.get(index).activityInfo.packageName);
                mFileInfo.appsNames.add(app.loadLabel(mActivity.getPackageManager()).toString());
                mFileInfo.appsIcons.add(mActivity.getPackageManager().getApplicationIcon(app));
            } catch (PackageManager.NameNotFoundException e) {}
            if(pkgAppsList.get(index).isDefault) mFileInfo.defaultApp = index;
        }

        return mFileInfo;
    }

    private class FtpFileInfo {
        public String name;
        public String type;
        public String size;
        public List<String> packages = new ArrayList<String>();
        public List<String> appsNames = new ArrayList<String>();
        public List<Drawable> appsIcons = new ArrayList<Drawable>();
        public int defaultApp;
    }
}
