package com.example.FTPClient.dialogs;

import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.FTPClient.Const;
import com.example.FTPClient.R;
import com.example.FTPClient.Request;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.entity.HeapFiles;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: Sergey Shuruta
 * Date: 12/25/13
 * Time: 12:00 PM
 */
public class DownloadToDialog extends FTPDialog {

    private static FTPApplication.ListType mListType;
    private EditText mTo;

    public static final DownloadToDialog create(FragmentActivity activity, FTPApplication.ListType listType) {
        DownloadToDialog dialog = new DownloadToDialog();
        dialog.mListType = listType;
        dialog.show(activity.getSupportFragmentManager(), dialog.getTitle());
        return dialog;
    }

    public DownloadToDialog() {}

    @Override
    protected View onCreateView() {
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_download_to, null);
        ((TextView) view.findViewById(R.id.list_type)).setVisibility(View.INVISIBLE);
        String load_to = new String();
        if(mListType == FTPApplication.ListType.LOCAL) {
            load_to = FTPApplication.getInstance().getFTPManager().getFTPCurrentDir();
            if(load_to.isEmpty()) load_to = File.separator;
            ((TextView) view.findViewById(R.id.label_load_to)).setText(R.string.upload_to_tp);
        } else {
            load_to = FTPApplication.getInstance()
                    .getSharedPreferences()
                    .getString(Const.SHARED_CURRENT_LOCAL_DIR,
                            FTPApplication.getInstance().getDownloadFolder()
                    );
            ((TextView) view.findViewById(R.id.label_load_to)).setText(R.string.download_to_tp);
        }
        mTo = (EditText) view.findViewById(R.id.load_to);
        mTo.setText(load_to);
        return view;
    }

    @Override
    protected String getTitle() {
        if(!isAdded()) return null;
        return getString(mListType == FTPApplication.ListType.FTP
                ? R.string.downloading
                : R.string.uploading);
    }

    @Override
    protected FTPApplication.ListType getListType() {
        return mListType;
    }

    @Override
    protected void onClickDoFTP() {
        String downloadTo = mTo.getText().toString();
        // TODO check dir is present
        HeapFiles heapFiles = FTPApplication.getInstance().getFTPManager().getDownloadNode();
        if(downloadTo.substring(downloadTo.length() - 1, downloadTo.length()).equals(File.separator))
            downloadTo = downloadTo.substring(0, downloadTo.length() - 1);
        heapFiles.setDownloadTo(downloadTo);
        FTPApplication.getInstance().getFTPManager().startDownload();
    }

    @Override
    protected void onClickDoLocal() {

    }

    @Override
    protected List<Request> getRequestsError() {
        return null;
    }

    @Override
    protected List<Request> getRequestsFinish() {
        List<Request> result = new ArrayList<>();
        result.add(Request.REQUEST_DOWNLOAD_POOL_START);
        return result;
    }

    @Override
    protected String getErrorMessage() {
        return null;
    }

    @Override
    protected int getPositiveBtnName() {
        return mListType == FTPApplication.ListType.FTP
                ? R.string.download
                : R.string.upload;
    }

    @Override
    protected void onCancelFTP() {
        super.onCancelFTP();
        FTPApplication.getInstance().getFTPManager().clearDownload();
    }
}
