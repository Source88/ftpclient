package com.example.FTPClient.dialogs;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.FTPClient.R;
import com.example.FTPClient.Request;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.events.MessageEvent;

import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by Sergey Shuruta on 6/17/14 at 6:13 PM.
 */
public abstract class FTPDialog extends DialogFragment {

    private View mView;
    private ProgressBar mProgress;

    protected abstract View onCreateView();
    protected abstract String getTitle();
    protected abstract FTPApplication.ListType getListType();
    protected abstract void onClickDoFTP();
    protected abstract void onClickDoLocal();
    protected abstract List<Request> getRequestsError();
    protected abstract List<Request> getRequestsFinish();
    protected abstract String getErrorMessage();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    final public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setCancelable(false);
        mView = onCreateView();
        mProgress = (ProgressBar) mView.findViewById(R.id.progress);
        TextView titleTextView = (TextView) mView.findViewById(R.id.title);
        String title = getTitle();
        if(title != null) {
            titleTextView.setText(title);
        } else {
            titleTextView.setVisibility(View.GONE);
        }

        String listTypeName = getString(
                getListType() == FTPApplication.ListType.FTP
                        ? R.string.ftp_list
                        : R.string.local_list);
        ((TextView) mView.findViewById(R.id.list_type)).setText(listTypeName);
        Button positiveBtn = (Button) mView.findViewById(R.id.positive_btn);
        positiveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getListType() == FTPApplication.ListType.FTP) {
                    onClickDoFTP();
                    mProgress.setVisibility(View.VISIBLE);
                } else {
                    onClickDoLocal();
                    getDialog().dismiss();
                    refreshList();
                }
            }
        });
        positiveBtn.setText(getPositiveBtnName());
        ((Button) mView.findViewById(R.id.negative_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getListType() == FTPApplication.ListType.FTP) {
                    onCancelFTP();
                } else {
                    onCancelLocal();
                }
            }
        });

        return mView;
    }

    public void onEventMainThread(MessageEvent event) {
        if(getRequestsError() != null && getRequestsError().contains(event.getAction())) {
            mProgress.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), getErrorMessage(), Toast.LENGTH_SHORT).show();
        }
        if(getRequestsFinish() != null && getRequestsFinish().contains(event.getAction())) {
            mProgress.setVisibility(View.INVISIBLE);
            getDialog().dismiss();
            refreshList();
        }
    }

    private void refreshList() {
        EventBus.getDefault().post(new MessageEvent(Request.EVENT_REFRESH_LIST));
    }

    protected int getPositiveBtnName() {
        return R.string.ok;
    }

    protected void onCancelFTP() {
        getDialog().dismiss();
    }

    protected void onCancelLocal() {
        getDialog().dismiss();
    }
}
