package com.example.FTPClient.dialogs;

import android.content.res.Resources;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import com.example.FTPClient.applications.FTPApplication;

import java.io.Serializable;

/**
 * Created by Sergey Shuruta on 6/19/14 at 11:10 AM.
 */
public interface DialogPattern extends Serializable {

    public interface ButtonSettings {
        public String getTitle(Resources resources);
        public void onClick();
    }

    public void onCreate(FragmentActivity activity);
    public String getTitle(Resources resources);
    public FTPApplication.ListType getListType();
    public boolean isCancelable();
    public ButtonSettings getPositiveBtn();
    public ButtonSettings getNegativeBtn();
    public View getView(FragmentActivity activity);
    public boolean isListType(FTPApplication.ListType listType);
}
