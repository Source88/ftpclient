/*
package com.example.FTPClient.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.example.FTPClient.R;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.entity.HeapFiles;
import com.example.FTPClient.entity.FTPFileNode;
import com.example.FTPClient.events.MessageEvent;
import com.example.FTPClient.managers.FTPManager;
import de.greenrobot.event.EventBus;

*/
/**
 * Author: Sergey Shuruta
 * Date: 12/25/13
 * Time: 12:00 PM
 *//*

public class DownloadDialog {

    private Context mContext;
    private Holder mHolder;
    private AlertDialog mDownloadDialog;
    private EventBus mEventBus;
    private FTPManager mFTPManager;
    private HeapFiles mHeapFiles;

    public static final String TAG = DownloadDialog.class.getSimpleName();

    public static DownloadDialog create(Context context) {
        return new DownloadDialog(context);
    }

    protected DownloadDialog(Context context) {
        mContext = context;
        mEventBus = EventBus.getDefault();
        mEventBus.register(this);
        mFTPManager = FTPApplication.getInstance().getFTPManager();
    }

    public void onEventMainThread(MessageEvent event) {
        FTPFileNode mFile;
        mHeapFiles = mFTPManager.getDownloadNode();
        switch (event.getAction()) {
            case REQUEST_DOWNLOAD_POOL_START:
                Log.d(TAG, "Download pull files start");
                break;
            case REQUEST_DOWNLOAD_FILE_START:
                if(mHolder.fileInfo != null)
                    mHolder.fileInfo.setText(mHeapFiles.getDownloadingFile().getName());
                break;
*/
/*            case REQUEST_DOWNLOAD_FILE_PROGRESS:
                if(mDownloadNode != null) {
                    mFile = mDownloadNode.getDownloadingFile();
                    mHolder.progressBar.setProgress(mFile.getProgress());
                    mHolder.progressInfo.setText(mFile.getProgress() + "%");
                }
                break;*//*

            case REQUEST_DOWNLOAD_FILE_ABORTED:
                mDownloadDialog.dismiss();
                break;
            case REQUEST_DOWNLOAD_POOL_FINISH:
                mDownloadDialog.dismiss();
                break;
            case REQUEST_DOWNLOAD_POOL_ERROR:
                mDownloadDialog.dismiss();
                break;
            case REQUEST_DOWNLOAD_FILE_PRESENT:
                mDownloadDialog.dismiss();
                break;
        }
    }

    public void destroy() {
        mEventBus.unregister(this);
    }

    public void show() {
        if(mDownloadDialog == null) {
            mHolder = new Holder();
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setTitle(null);
            builder.setView(mHolder.getView())
                    .setPositiveButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // TODO Stopped download
                            mDownloadDialog.dismiss();
                        }
                    })
                    */
/*.setNegativeButton(R.string.hide, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            mDownloadDialog.dismiss();
                        }
                    })*//*
;
            mDownloadDialog = builder.create();
        }
        mDownloadDialog.show();
    }

    private class Holder {
        private View mView;
        public ProgressBar progressBar;
        public TextView fileInfo;
        public TextView progressInfo;
        public LinearLayout downloadDialog;

        public Holder() {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mView = inflater.inflate(R.layout.dialog_download, null);
            initGUI();
        }

        private void initGUI() {
            fileInfo = (TextView) mView.findViewById(R.id.info_file);
            progressBar = (ProgressBar) mView.findViewById(R.id.progress_bar);
            progressInfo = (TextView) mView.findViewById(R.id.info_progress);
            downloadDialog = (LinearLayout) mView.findViewById(R.id.download_dialog);
        }

        public View getView() {
            return mView;
        }
    }

}
*/
