package com.example.FTPClient.dialogs;

import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.EditText;

import com.example.FTPClient.entity.FileUnit;
import com.example.FTPClient.R;
import com.example.FTPClient.Request;
import com.example.FTPClient.applications.FTPApplication;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergey Shuruta on 6/19/14 at 12:09 PM.
 */
public class RenameDialog extends FTPDialog {

    private static FileUnit mFile;
    private static FTPApplication.ListType mListType;
    private EditText mNewNameEdit;

    public static final RenameDialog create(FragmentActivity activity, FTPApplication.ListType listType, FileUnit file) {
        RenameDialog dialog = new RenameDialog();
        dialog.mListType = listType;
        dialog.mFile = file;
        dialog.show(activity.getSupportFragmentManager(), dialog.getTitle());
        return dialog;
    }

    public RenameDialog() {}

    @Override
    protected View onCreateView() {
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_rename, null);
        mNewNameEdit = (EditText) view.findViewById(R.id.new_name);
        mNewNameEdit.setText(mFile.getName());
        mNewNameEdit.selectAll();
        return view;
    }

    @Override
    protected String getTitle() {
        if(!isAdded()) return null;
        return getString(R.string.rename_file, mFile.getName());
    }

    @Override
    protected int getPositiveBtnName() {
        return R.string.rename;
    }

    @Override
    public FTPApplication.ListType getListType() {
        return mListType;
    }

    @Override
    protected void onClickDoFTP() {
        FTPApplication.getInstance().getFTPManager().renameFile(mFile, mNewNameEdit.getText().toString());
    }

    @Override
    protected void onClickDoLocal() {
        String fileDir = File.separator;
        for(String dir : mFile.getLocalDir().split(File.separator))
            if(!dir.equals(mFile.getName()))
                fileDir += dir + File.separator;
        File from = new File(fileDir, mFile.getName());
        File to = new File(fileDir, mNewNameEdit.getText().toString());
        from.renameTo(to);
    }

    @Override
    protected List<Request> getRequestsError() {
        List<Request> result = new ArrayList<>();
        result.add(Request.REQUEST_RENAME_FILE_FILED);
        return result;
    }

    @Override
    protected List<Request> getRequestsFinish() {
        List<Request> result = new ArrayList<>();
        result.add(Request.REQUEST_RENAME_FILE_FINISH);
        return result;
    }

    @Override
    protected String getErrorMessage() {
        return getString(mFile.isDir()
                ? R.string.rename_dir_error
                : R.string.rename_file_error, mFile.getName());
    }
}
