package com.example.FTPClient.dialogs;

import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.FTPClient.R;
import com.example.FTPClient.Request;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.entity.HeapFiles;
import com.example.FTPClient.events.MessageEvent;

import java.util.List;

/**
 * Author: Sergey Shuruta
 * Date: 12/25/13
 * Time: 12:00 PM
 */
public class DownloadDialog2 extends FTPDialog {

    private static FTPApplication.ListType mListType;
    private ProgressBar mProgress;
    private TextView mInfo, mDesc;
    private HeapFiles mHeapFiles;

    public static final DownloadDialog2 create(FragmentActivity activity, FTPApplication.ListType listType) {
        DownloadDialog2 dialog = new DownloadDialog2(listType);
        dialog.show(activity.getSupportFragmentManager(), dialog.getTitle());
        return dialog;
    }

    public DownloadDialog2() {}

    public DownloadDialog2(FTPApplication.ListType listType) {
        mListType = listType;
        mHeapFiles = FTPApplication.getInstance().getFTPManager().getDownloadNode();
    }

    @Override
    protected View onCreateView() {
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_download, null);
        mProgress = (ProgressBar) view.findViewById(R.id.progress);
        mInfo = (TextView) view.findViewById(R.id.info);
        mDesc = (TextView) view.findViewById(R.id.desc);
        return view;
    }

    @Override
    protected String getTitle() {
        if(!isAdded() && mListType != null) return null;
        return getString(mListType == FTPApplication.ListType.FTP
                ? R.string.download_to_tp
                : R.string.upload_to_tp) + " " + mHeapFiles.getDownloadTo();
    }

    @Override
    protected FTPApplication.ListType getListType() {
        return mListType;
    }

    @Override
    protected void onClickDoFTP() {

    }

    @Override
    protected void onClickDoLocal() {

    }

    @Override
    protected List<Request> getRequestsError() {
        return null;
    }

    @Override
    protected List<Request> getRequestsFinish() {
        return null;
    }

    @Override
    protected String getErrorMessage() {
        return null;
    }

    public void onEventMainThread(MessageEvent event) {
        switch (event.getAction()) {
            case REQUEST_DOWNLOAD_FILE_START:
                mDesc.setText(mHeapFiles.getDownloadingFile().getName());
                break;
            case REQUEST_DOWNLOAD_FILE_PROGRESS:
                Log.d("TEST", "Progress: " + mHeapFiles.getProgress());
                mProgress.setProgress(mHeapFiles.getProgress());
                mInfo.setText(mHeapFiles.getProgress() + "%");
                break;
/*            case REQUEST_DOWNLOAD_FILE_ABORTED:
                dismiss();
                break;
            case REQUEST_DOWNLOAD_POOL_FINISH:
                dismiss();
                break;
            case REQUEST_DOWNLOAD_POOL_ERROR:
                dismiss();
                break;
            case REQUEST_DOWNLOAD_FILE_PRESENT:
                dismiss();
                break;*/
        }
    }

    @Override
    protected int getPositiveBtnName() {
        return R.string.hide;
    }
}
