package com.example.FTPClient.dialogs;

import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.FTPClient.R;
import com.example.FTPClient.Request;
import com.example.FTPClient.applications.FTPApplication;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergey Shuruta on 6/19/14 at 1:36 PM.
 */
public class NewFolderDialog extends FTPDialog {

    private EditText mNewFolderNameEdit;
    private static FTPApplication.ListType mListType;
    private static File mCreateIn;

    public static final NewFolderDialog create(FragmentActivity activity, FTPApplication.ListType listType, File createIn) {
        NewFolderDialog dialog = new NewFolderDialog();
        dialog.mListType = listType;
        dialog.mCreateIn = createIn;
        dialog.show(activity.getSupportFragmentManager(), dialog.getTitle());
        return dialog;
    }

    public NewFolderDialog() {}

    @Override
    protected View onCreateView() {
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_new_folder, null);
        mNewFolderNameEdit = (EditText) view.findViewById(R.id.new_folder_name);
        return view;
    }

    @Override
    protected String getTitle() {
        if(!isAdded()) return null;
        return getString(R.string.create_new_folder);
    }

    @Override
    protected FTPApplication.ListType getListType() {
        return mListType;
    }

    @Override
    protected void onClickDoFTP() {
        Log.d("TEST", "NewFolderDialog2: (FTP) " + getNewDirPath());
        FTPApplication.getInstance().getFTPManager().createFolder(getNewDirPath());
    }

    @Override
    protected void onClickDoLocal() {
        Log.d("TEST", "NewFolderDialog2: (LOCAL) " + getNewDirPath());
        getNewDirPath().mkdirs();
    }

    @Override
    protected List<Request> getRequestsError() {
        List<Request> result = new ArrayList<>();
        result.add(Request.REQUEST_FOLDER_CREATED_FILED);
        return result;
    }

    @Override
    protected List<Request> getRequestsFinish() {
        List<Request> result = new ArrayList<>();
        result.add(Request.REQUEST_FOLDER_CREATED_FINISH);
        return result;
    }

    @Override
    protected String getErrorMessage() {
        return null;
    }

    private File getNewDirPath() {
        return new File(mCreateIn.getPath()
                .concat(File.separator)
                .concat(mNewFolderNameEdit.getText().toString())
                .concat(File.separator));
    }
}
