package com.example.FTPClient.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import com.example.FTPClient.R;

/**
 * Created by Sergey Shuruta on 1/31/14 at 3:51 PM.
 */
public class NoInternetDialog extends AlertDialog.Builder {

    private Activity mActivity;

    public static NoInternetDialog create(Activity activity) {
        return new NoInternetDialog(activity);
    }

    protected NoInternetDialog(Activity activity) {
        super(activity);
        mActivity = activity;
        setTitle(null);
        setMessage(R.string.no_intenet_connection);
        setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                mActivity.finish();
            }
        });
    }
}
