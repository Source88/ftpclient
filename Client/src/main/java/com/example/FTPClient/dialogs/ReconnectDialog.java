package com.example.FTPClient.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import com.example.FTPClient.R;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.managers.FTPManager;

/**
 * Created by Sergey Shuruta on 1/31/14 at 3:51 PM.
 */
public class ReconnectDialog extends AlertDialog.Builder {

    private Activity mActivity;
    private FTPManager mFTPManager;

    public static ReconnectDialog create(Activity activity) {
        return new ReconnectDialog(activity);
    }

    protected ReconnectDialog(Activity activity) {
        super(activity);
        mFTPManager = FTPApplication.getInstance().getFTPManager();
        mActivity = activity;
        setTitle(null);
        setMessage(R.string.connection_lost_ask_recover);
        setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                RefreshDialog.create(mActivity).show(R.string.reconnection_pts);
                mFTPManager.reconnect();
            }
        });
        setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mFTPManager.disconnect(false, FTPManager.Reconnect.NO_AND_CLOSE);
                dialog.dismiss();
            }
        });
    }
}
