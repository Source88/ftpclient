package com.example.FTPClient.dialogs;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.example.FTPClient.R;
import com.example.FTPClient.applications.FTPApplication;
import de.greenrobot.event.EventBus;

import java.io.Serializable;

/**
 * Created by Sergey Shuruta on 6/19/14 at 11:08 AM.
 */
public class DialogFactory extends DialogFragment implements View.OnClickListener {

    private OnFinalizeListener mOnFinalizeListener;
    private DialogPattern mDialogPattern;
    private View mView;
    private ProgressBar mProgress;

    public static class DialogEvent {
        private String mMessage;
        private EventType mEventType;

        public enum EventType {DONE, ERROR}

        public DialogEvent(EventType eventType) {
            this(eventType, "");
        }

        public DialogEvent(EventType eventType, String message) {
            mMessage = message;
            mEventType = eventType;
        }

        public String getMessage() {
            return mMessage;
        }

        public boolean isError() {
            return mEventType == EventType.ERROR;
        }

        public boolean isDone() {
            return mEventType == EventType.DONE;
        }
    }

    public interface OnFinalizeListener extends Serializable {
        public void finalize(DialogEvent event);
    }

    public static void display(FragmentActivity activity, DialogPattern dialog, OnFinalizeListener onFinalizeListener) {
        new DialogFactory(activity, dialog, onFinalizeListener).show(activity.getSupportFragmentManager(), dialog.getClass().getSimpleName());
    }

    public DialogFactory() {}

    private DialogFactory(FragmentActivity activity, final DialogPattern dialogPattern, OnFinalizeListener onFinalizeListener) {
        mOnFinalizeListener = onFinalizeListener;
        mDialogPattern = dialogPattern;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("mOnFinalizeListener", mOnFinalizeListener);
        outState.putSerializable("mDialogPattern", mDialogPattern);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDialogPattern.onCreate((FragmentActivity)getActivity());
        if(savedInstanceState != null) {
            mOnFinalizeListener = (OnFinalizeListener) savedInstanceState.getSerializable("mOnFinalizeListener");
            mDialogPattern = (DialogPattern) savedInstanceState.getSerializable("mDialogPattern");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setCancelable(mDialogPattern.isCancelable());
        mView = mDialogPattern.getView(getActivity());
        mProgress = (ProgressBar) mView.findViewById(R.id.progress);

        TextView titleTextView = (TextView) mView.findViewById(R.id.title);
        String title = mDialogPattern.getTitle(getActivity().getResources());
        if(title != null) {
            titleTextView.setText(title);
        } else {
            titleTextView.setVisibility(View.GONE);
        }

        String listTypeName = getString(
                mDialogPattern.getListType() == FTPApplication.ListType.FTP
                ? R.string.ftp_list
                : R.string.local_list);
                ((TextView) mView.findViewById(R.id.list_type)).setText(listTypeName);
        ((Button) mView.findViewById(R.id.positive_btn)).setOnClickListener(this);
        ((Button) mView.findViewById(R.id.negative_btn)).setOnClickListener(this);

        return mView;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.positive_btn:
                showLoader();
                mDialogPattern.getPositiveBtn().onClick();
                break;
            case R.id.negative_btn:
                mDialogPattern.getNegativeBtn().onClick();
                getDialog().cancel();
                break;
        }
    }


    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        mDialogPattern.getNegativeBtn().onClick();
        getDialog().dismiss();
    }

    public void onEventMainThread(DialogEvent event) {
        getDialog().dismiss();
        hideLoader();
        mOnFinalizeListener.finalize(event);
    }

    private void showLoader() {
        if(mDialogPattern.isListType(FTPApplication.ListType.FTP))
            mProgress.setVisibility(View.VISIBLE);
    }

    private void hideLoader() {
        mProgress.setVisibility(View.INVISIBLE);
    }

}
