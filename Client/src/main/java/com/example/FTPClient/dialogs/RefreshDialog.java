package com.example.FTPClient.dialogs;

import android.app.ProgressDialog;
import android.content.Context;
import com.example.FTPClient.events.MessageEvent;
import de.greenrobot.event.EventBus;

/**
 * Dialog for creating directory (ftp or local)
 * Created by Sergey Shuruta on 1/31/14.
 */
public class RefreshDialog extends ProgressDialog {

    private Context mContext;

    private RefreshDialog(Context context) {
        super(context);
        mContext = context;
        setIndeterminate(true);
        setCancelable(false);
        EventBus.getDefault().register(this);
    }

    /**
     * Created new instance of class
     * @param context
     */
    public static RefreshDialog create(Context context) {
        return new RefreshDialog(context);
    }

    @Override
    final public void show() {
        super.show();
    }


    public RefreshDialog show(int message) {
        return show(mContext.getString(message));
    }

    public RefreshDialog show(String message) {
        setMessage(message);
        show();
        return this;
    }

    @Override
    public void dismiss() {
        super.dismiss();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(MessageEvent event) {
        //Log.d("TEST", "onEventMainThread(" + event.getAction() + ")");
        switch (event.getAction()) {
            case REQUEST_READ_LIST_FINISH:
                dismiss();
                break;
        }
    }
}
