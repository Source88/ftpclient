package com.example.FTPClient.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import com.example.FTPClient.R;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.managers.FTPManager;

/**
 * Author: Sergey Shuruta
 * Date: 12/25/13
 * Time: 12:00 PM
 */
public class FilePresentDialog extends AlertDialog.Builder {

    private final static String TAG = FilePresentDialog.class.getSimpleName();

    public static FilePresentDialog create(Activity activity) {
        return new FilePresentDialog(activity);
    }

    protected FilePresentDialog(Context context) {
        super(context);
        final FTPManager ftpPManager = FTPApplication.getInstance().getFTPManager();
        //final FTPFileNode file = ftpPManager.getDownloadNode().getDownloadingFile();
        setTitle(null);
        //setMessage(String.format(context.getString(R.string.file_is_present_message), file.getName()));
        setCancelable(false);
        setPositiveButton(R.string.replace, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Log.d(TAG, "Add to replace list: " + file.getName());
                //ftpPManager.getDownloadNode().setReplaceFile(file.getLocalDir() + File.separator + file.getName());
                ftpPManager.startDownload();
                dialog.dismiss();
            }
        });
        setNeutralButton(R.string.replace_all, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ftpPManager.getDownloadNode().setReplaceAll();
                ftpPManager.startDownload();
                dialog.dismiss();
            }
        });
        setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public AlertDialog.Builder setTitle(int titleId) {
        return super.setTitle(titleId);
    }
}
