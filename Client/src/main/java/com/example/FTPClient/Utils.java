package com.example.FTPClient;

import android.content.Context;
import android.content.res.Configuration;
import com.example.FTPClient.applications.FTPApplication;
import it.sauronsoftware.ftp4j.FTPFile;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Source on 17.04.2014.
 */
public class Utils {

    public static String getFormatSize(FTPFile file) {
        if(file.getType() == FTPFile.TYPE_DIRECTORY) return new String();
        long size = file.getSize();
        return getFormatSize(size);
    }

    public static String getFormatSize(long size) {
        if(size <= 0) return "0";
        final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    public static boolean isLandscapeOrientation() {
        Context context = FTPApplication.getInstance().getApplicationContext();
        if(context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
            return false;
        else if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            return true;
        else
            return false;
    }

    public static boolean contain(List<FTPFile> ftpFiles, String s) {
        for (FTPFile file : ftpFiles) {
            if(file.getName().equals(s))
                return true;
        }
        return false;
    }

    public static List<String> getListFromStr(String localDirStr, String separator) {
        return new ArrayList<String>(Arrays.asList(localDirStr.split(separator)));
    }
}
