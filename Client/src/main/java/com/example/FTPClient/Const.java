package com.example.FTPClient;

/**
 * Author: Sergey Shuruta
 * Date: 11/29/13
 * Time: 2:06 PM
 */
public class Const {
    public static final String SHARED_PREFERENCES_NAME = "ftp_client_shared_preferences";
    public static final String SHARED_CURRENT_LOCAL_DIR = "current_local_dir";
    public static final String CONNECTION_SELECTED_ID = "connection_selected";
    public static final int PARAM_RECONNECT = 1;

    public static final int FTP_BEGIN_INITIATED = 100; //Запрошенное действие инициировано, дождитесь следующего ответа прежде, чем выполнять новую команду.
    public static final int FTP_COMMENT = 120; //Комментарий
    public static final int FTP_READY_IN_NNN_MINUTES = 120; //Функция будет реализована через nnn минут
    public static final int FTP_CONNECTION_ALREADY_OPEN = 125; //Канал открыт, обмен данными начат
    public static final int FTP_STATUS_OKAY = 150; //Статус файла правилен, подготавливается открытие канала
    public static final int FTP_SUCCESSFULLY_COMPLETED = 200; //Команда корректна
    public static final int FTP_COMMAND_NOT_IMPLEMENTED = 202; //Команда не поддерживается
    public static final int FTP_SYSTEM_STATUS = 211; //Системный статус или отклик на справочный запрос
    public static final int FTP_DIRECTORY_STATUS = 212; //Состояние каталога
    public static final int FTP_FILE_STATUS = 213; //Состояние файла
    public static final int FTP_HELP_MESSAGE = 214; //Справочное поясняющее сообщение
    public static final int FTP_NAME_SYSTEM_TYPE = 215; //Выводится вместе с информацией о системе по команде SYST
    public static final int FTP_READY_FOR_NEW_USER = 220; //Служба готова для нового пользователя.
    public static final int FTP_SERVICE_CLOSING = 221; //Благополучное завершение по команде quit
    public static final int FTP_CONNECTION_OPEN_NO_TRANSFER = 225; //Канал сформирован, но информационный обмен отсутствует
    public static final int FTP_CLOSING_DATA_CONNECTION = 226; //Закрытие канала, обмен завершен успешно
    public static final int FTP_ENTERING_PASSIVE_MODE = 227; //Переход в пассивный режим (h1,h2,h3,h4,p1,p2).
    public static final int FTP_ENTERING_PASSIVE_MODE_LONG = 228; //Переход в длинный пассивный режим (длинный адрес, порт).
    public static final int FTP_ENTERING_PASSIVE_MODE_EXTENDED = 229; //Переход в расширенный пассивный режим (|||port|).
    public static final int FTP_USER_LOGGED_IN = 230; //Пользователь идентифицирован, продолжайте
    public static final int FTP_USER_LOGGED_OUT = 231; //Пользовательский сеанс окончен; Обслуживание прекращено.
    public static final int FTP_LOGOUT_COMMAND_NOTED = 232; //Команда о завершении сеанса принята, она будет завершена по завершении передачи файла.
    public static final int FTP_REQUESTED_ACTION_OKAY = 250; //Запрос прошёл успешно
    public static final int FTP_PATHNAME_CREATED = 257; //«ПУТЬ» создан.
    public static final int FTP_ACCEPTED_BUT_REQUESTED_HOLD = 300; //Команда была воспринята, но запрошенные действия находится на удержании, до получения дополнительной информации.
    public static final int FTP_USER_NAME_OKAY_NEED_PASS = 331; //Имя пользователя корректно, нужен пароль
    public static final int FTP_NEED_ACCOUNT_FOR_LOGIN = 332; //Для входа в систему необходима аутентификация
    public static final int FTP_REQUESTED_ACTION_PENDING_MORE_INF = 350; //Запрошенное действие над файлом требует большей информации
    public static final int FTP_SERVER_NOT_FOUNDED = 404; //Данный удалённый сервер не найден
    public static final int FTP_SERVICE_NOT_AVAILABLE = 421; //Процедура не возможна, канал закрывается
    public static final int FTP_CAN_NOT_OPEN_CONNECTION = 425; //Открытие информационного канала не возможно
    public static final int FTP_CONNECTION_CLOSED_TRANSFER_ABORTED = 426; //Канал закрыт, обмен прерван
    public static final int FTP_INVALID_USERNAME_OR_PASS = 430; //Invalid username or password
    public static final int FTP_REQUESTED_HOST_UNAVAILABLE = 434; //Запрашиваемый хост недоступен
    public static final int FTP_REQUESTED_ACTION_NOT_TAKEN = 450; //Запрошенная функция не реализована, файл не доступен, например, занят
    public static final int FTP_REQUESTED_ACTION_ABORTED = 451; //Локальная ошибка, операция прервана
    public static final int FTP_INSUFFICIENT_STORAGE_SPACE = 452; //Ошибка при записи файла (недостаточно места)
    public static final int FTP_SYNTAX_ERROR = 500; //Синтаксическая ошибка, команда не может быть интерпретирована (возможно она слишком длинна)
    public static final int FTP_SYNTAX_ERROR_IN_PARAMS_OR_ARGS = 501; //Синтаксическая ошибка (неверный параметр или аргумент)
    public static final int FTP_COMMAND_NOT_IMPLEMENTED_MODE = 502; //Команда не используется (нелегальный тип MODE)
    public static final int FTP_BAD_SEQUENCE_OF_COMMANDS = 503; //Неудачная последовательность команд
    public static final int FTP_COMMAND_NOT_IMPLEMENTED_FOR_THAT_PARAMETER = 504; //Команда не применима для такого параметра
    public static final int FTP_NOT_LOGGED_IN = 530; //Вход не выполнен! Требуется авторизация (not logged in)
    public static final int FTP_NEED_ACCOUNT_FOR_STORING_FILES = 532; //Необходима аутентификация для запоминания файла
    public static final int FTP_REQUESTED_ACTION_NOT_TAKEN_FILE_NOT_FOUND = 550; //Запрошенная функция не реализована, файл не доступен, например, не найден
    public static final int FTP_PAGE_TYPE_UNKNOWN_ABORTED = 551; //Запрошенная операция прервана. Неизвестный тип страницы.
    public static final int FTP_EXCEEDED_STORAGE_ALLOCATION_ABORTED = 552; //Запрошенная операция прервана. Выделено недостаточно памяти
    public static final int FTP_FILE_NAME_NOT_ALLOWED = 553; //Запрошенная операция не принята. Недопустимое имя файла.
}
