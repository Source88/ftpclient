package com.example.FTPClient.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.FTPClient.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author: Sergey Shuruta
 * Date: 10/17/13
 * Time: 5:15 PM
 */
public class LocalListAdapter extends ArrayAdapter<File> {

    private Context mContext;
    private HashMap<Integer, Boolean> mSelected = new HashMap<Integer, Boolean>();
    private List<File> mFiles = new ArrayList<File>();

    public LocalListAdapter(Context context, List<File> files) {
        super(context, R.layout.row_local_list, files);
        mContext = context;
        mFiles = files;
    }

    @Override
    public int getCount() {
        return mFiles.size();
    }

    @Override
    public File getItem(int position) {
        return mFiles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<Integer> getSelected() {
        List<Integer> selected = new ArrayList<Integer>();

        for (Map.Entry<Integer, Boolean> entry : mSelected.entrySet()) {
            if(entry.getValue()) {
                selected.add(entry.getKey());
            }
        }
        return selected;
    }

    public List<File> getSelectedFiles() {
        List<File> selectedFiles = new ArrayList<File>();
        for (Map.Entry<Integer, Boolean> entry : mSelected.entrySet()) {
            if(entry.getValue()) {
                selectedFiles.add(getItem(entry.getKey()));
            }
        }
        return selectedFiles;
    }

    public void clearSelection() {
        mSelected.clear();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            rowView = inflater.inflate(R.layout.row_file, null);
            holder = new ViewHolder();
            holder.icon = (ImageView) rowView.findViewById(R.id.icon);
            holder.name = (TextView) rowView.findViewById(R.id.name);
            holder.date = (TextView) rowView.findViewById(R.id.date);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        File file = getItem(position);
        if(!file.getName().equals("..")) {
            holder.name.setText(file.getName());
            SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            String formatted = format1.format(file.lastModified());
            holder.date.setText(formatted);
            holder.date.setVisibility(View.VISIBLE);
            holder.icon.setImageResource(file.isFile() ? R.drawable.ic_file : R.drawable.ic_folder);
        } else {
            holder.name.setText("..");
            holder.date.setVisibility(View.GONE);
            holder.icon.setImageResource(R.drawable.ic_back);
        }

        return rowView;
    }

    private class ViewHolder {
        public ImageView icon;
        public TextView name;
        public TextView size;
        public TextView date;
    }
}
