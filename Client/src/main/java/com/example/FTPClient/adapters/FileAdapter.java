package com.example.FTPClient.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.FTPClient.entity.FileUnit;
import com.example.FTPClient.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergey Shuruta on 5/5/14 at 4:04 PM.
 */
public class FileAdapter extends ArrayAdapter<FileUnit> {

    private List<FileUnit> mSelected = new ArrayList<FileUnit>();
    private View.OnClickListener mPopupListener;
    private View.OnClickListener mSelectListener;

    private static final int ROW_LAYOUT = R.layout.row_file;

    public FileAdapter(Context context, List<FileUnit> files, View.OnClickListener selectListener, View.OnClickListener popupListener) {
        super(context, ROW_LAYOUT, files);
        mSelectListener = selectListener;
        mPopupListener = popupListener;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            rowView = inflater.inflate(ROW_LAYOUT, null);
            holder = new ViewHolder();
            holder.view = rowView;
            holder.icon = (ImageView) rowView.findViewById(R.id.icon);
            holder.name = (TextView) rowView.findViewById(R.id.name);
            holder.size = (TextView) rowView.findViewById(R.id.size);
            holder.date = (TextView) rowView.findViewById(R.id.date);
            holder.popupBtn = (ImageView) rowView.findViewById(R.id.popup_btn);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        FileUnit file = getItem(position);
        holder.name.setText(file.getName());
        holder.date.setText(file.getDate());
        holder.size.setText(file.getSize());
        holder.icon.setTag(getHash(position));
        holder.icon.setImageDrawable(file.getIcon());
        if(mSelectListener != null)
            holder.icon.setOnClickListener(mSelectListener);
        holder.isSelected(mSelected.contains(getFileByHash(getHash(position))));
        holder.popupBtn.setOnClickListener(mPopupListener);
        holder.popupBtn.setTag(getHash(position));

        return rowView;
    }

    private class ViewHolder {
        public View view;
        public ImageView icon;
        public TextView name;
        public TextView size;
        public TextView date;
        public ImageView popupBtn;

        public void isSelected(boolean isSelected) {
            if (isSelected) {
                view.setBackgroundColor(getContext().getResources().getColor(R.color.select_item_color));
            } else {
                view.setBackgroundColor(getContext().getResources().getColor(android.R.color.transparent));
            }
        }
    }

    public void select(View view) {
        FileUnit file = getFileByHash((String) view.getTag());
        if(!mSelected.contains(file)) {
            mSelected.add(file);
        } else {
            mSelected.remove(file);
        }
        notifyDataSetChanged();
    }

    public int getSelectedCount() {
        return mSelected.size();
    }

    public void clearSelection() {
        mSelected.clear();
    }

    public List<FileUnit> getSelected() {
        return new ArrayList<FileUnit>(mSelected);
    }

    public FileUnit getSelected(int position) {
        return mSelected.get(position);
    }


    private FileUnit getFileByHash(String hash) {
        return getItem(Integer.valueOf(hash.split("_")[1]));
    }

    private String getHash(int position) {
        return "hash" + "_" + position;
    }

}
