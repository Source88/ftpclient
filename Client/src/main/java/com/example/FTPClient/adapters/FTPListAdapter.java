package com.example.FTPClient.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.FTPClient.R;
import com.example.FTPClient.Utils;
import com.example.FTPClient.activities.SessionActivity;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.entity.FTPFileNode;
import com.example.FTPClient.managers.FTPManager;
import it.sauronsoftware.ftp4j.FTPFile;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: Sergey Shuruta
 * Date: 10/17/13
 * Time: 5:15 PM
 */
public class FTPListAdapter extends ArrayAdapter<FTPFile> {

    private List<String> mSelected = new ArrayList<String>();
    private List<FTPFile> mFtpFiles;
    private SessionActivity mActivity;
    private FTPManager mFTPManager;
    private View.OnClickListener mPopupListener;
    private View.OnClickListener mSelectListener;



    public FTPListAdapter(SessionActivity activity, List<FTPFile> ftpFiles, View.OnClickListener popupListener, View.OnClickListener selectListener) {
        super(activity, R.layout.row_file, ftpFiles);
        //mFtpFiles = ftpFiles;
        mActivity = activity;
        mFTPManager = FTPApplication.getInstance().getFTPManager();
        mPopupListener = popupListener;
        mSelectListener = selectListener;
        mFtpFiles = ftpFiles;
    }

    @Override
    public int getCount() {
        if(!Utils.isLandscapeOrientation() && mFtpFiles.size() > 0) {
            return mFtpFiles.size() - 1;
        }
        return mFtpFiles.size();
    }

    @Override
    public FTPFile getItem(int position) {
        if(!Utils.isLandscapeOrientation()) {
            return mFtpFiles.get(position + 1);
        }
        return mFtpFiles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void addAll(List<FTPFile> collection) {
        for(FTPFile file : collection) {
            super.add(file);
        }
    }

/*    @Override
    public void clear() {
        super.clear();
        mFtpFiles.clear();
    }*/

/*
    @Override
    public void notifyDataSetChanged() {
        mFtpFiles = FTPApplication.getInstance().getFTPManager().getListFiles();
        super.notifyDataSetChanged();
    }
*/

    public void clearSelection() {
        mSelected.clear();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = LayoutInflater.from(mActivity);
            rowView = inflater.inflate(R.layout.row_file, null);
            holder = new ViewHolder();
            holder.view = rowView;
            holder.icon = (ImageView) rowView.findViewById(R.id.icon);
            holder.name = (TextView) rowView.findViewById(R.id.name);
            holder.size = (TextView) rowView.findViewById(R.id.size);
            holder.date = (TextView) rowView.findViewById(R.id.date);
            holder.popupBtn = (ImageView) rowView.findViewById(R.id.popup_btn);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        FTPFile file = getItem(position);
        if(!file.getName().equals("..")) {
            holder.name.setText(file.getName());
            holder.date.setText(getDate(file));
            holder.size.setText(Utils.getFormatSize(file));
            holder.icon.setTag("hash" + "_" + position);
            holder.icon.setImageResource(file.getType() == FTPFile.TYPE_FILE ? R.drawable.ic_file : R.drawable.ic_folder);
            holder.icon.setOnClickListener(mSelectListener);
            holder.isSelected(mSelected.contains("hash" + "_" + position));
            holder.popupBtn.setOnClickListener(mPopupListener);
            holder.popupBtn.setTag("hash" + "_" + position);
        } else {
            holder.name.setText("..");
            holder.date.setVisibility(View.GONE);
            holder.icon.setImageResource(R.drawable.ic_back);
        }

        return rowView;
    }

    public void setSelected(String hash) {
        if(!mSelected.contains(hash)) {
            mSelected.add(hash);
        } else {
            mSelected.remove(hash);
        }
        notifyDataSetChanged();
    }

    private class ViewHolder {
        public View view;
        public ImageView icon;
        public TextView name;
        public TextView size;
        public TextView date;
        public ImageView popupBtn;


        public void isSelected(boolean isSelected) {
            if (isSelected) {
                view.setBackgroundColor(mActivity.getResources().getColor(R.color.select_item_color));
            } else {
                view.setBackgroundColor(mActivity.getResources().getColor(android.R.color.transparent));
            }
        }
    }

    private String getDate(FTPFile file) {
        SimpleDateFormat setFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        return setFormat.format(file.getModifiedDate());
    }


    public List<FTPFileNode> getSelected() {
        List<FTPFileNode> selectedFiles = new ArrayList<FTPFileNode>();
        for (String select : mSelected) {
            FTPFileNode file = new FTPFileNode(getItem(Integer.valueOf(select.split("_")[1])));
            file.setId(selectedFiles.size());
            file.setFTPDir(mFTPManager.getFTPCurrentDir());
            //file.setLocalDir(mFTPManager.getConnection().getLocalDir());
            selectedFiles.add(file);
        }
        return selectedFiles;
    }
}
