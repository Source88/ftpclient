package com.example.FTPClient.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.FTPClient.Const;
import com.example.FTPClient.R;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.entity.Connection;

import java.util.List;

/**
 * Author: Sergey Shuruta
 * Date: 10/22/13
 * Time: 10:18 AM
 */
public class ConsListAdapter extends ArrayAdapter<Connection> {

    private Context mContext;
    private List<Connection> mConnections;
    private long mSelectedItem;

    public ConsListAdapter(Context context, List<Connection> connections) {
        super(context, R.layout.dialog_connections_list_row, connections);
        mContext = context;
        mConnections = connections;
        mSelectedItem = FTPApplication.getInstance().getSharedPreferences().getLong(Const.CONNECTION_SELECTED_ID, 0);
    }

    @Override
    public Connection getItem(int position) {
        return mConnections.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mConnections.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            rowView = inflater.inflate(R.layout.dialog_connections_list_row, null);
            holder = new ViewHolder(rowView);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        Connection connection = getItem(position);
        holder.displayConnection(connection);
        return rowView;
    }

    private class ViewHolder {
        private View mView;
        private LinearLayout mRoot;
        private TextView mName;
        private TextView mServer;

        private ViewHolder(View view) {
            mView = view;
            mRoot = (LinearLayout) mView.findViewById(R.id.root);
            mName = (TextView) mView.findViewById(R.id.name);
            mServer = (TextView) mView.findViewById(R.id.server);
        }

        public void displayConnection(Connection connection) {
            mName.setText(connection.getName());
            mServer.setText(connection.getServer());
            if(connection.getId() == mSelectedItem)
                mRoot.setBackgroundColor(getContext().getResources().getColor(R.color.select_item_color));
            else
                mRoot.setBackgroundColor(getContext().getResources().getColor(android.R.color.transparent));
        }
    }
}
