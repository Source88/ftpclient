package com.example.FTPClient.applications;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Environment;
import com.example.FTPClient.Const;
import com.example.FTPClient.managers.FTPManager;

/**
 * Created with IntelliJ IDEA.
 * User: Source
 * Date: 13.10.13
 * Time: 16:04
 * To change this template use File | Settings | File Templates.
 */
public class FTPApplication extends Application {

    private static FTPApplication mInstance;
    private FTPManager mFTPManager;
    private SharedPreferences mSharedPreferences;
    private ListType mSelectedList = ListType.FTP;

    public enum ListType {FTP, LOCAL};

    public static FTPApplication getInstance() {
        if (mInstance == null) {
            synchronized (FTPApplication.class) {
                if (mInstance == null) {
                    throw new IllegalStateException("Application is not started.");
                }
            }
        }
        return mInstance;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        mSharedPreferences = getSharedPreferences(Const.SHARED_PREFERENCES_NAME, MODE_PRIVATE);
        mFTPManager = new FTPManager(getApplicationContext());
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public FTPManager getFTPManager() {
        return mFTPManager;
    }

    public SharedPreferences getSharedPreferences() {
        return mSharedPreferences;
    }

    public String getDownloadFolder() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
    }

    public ListType getSelectedPanel() {
        return mSelectedList;
    }

    public void setSelectedPanel(ListType selectedList) {
        mSelectedList = selectedList;
    }

}
