package com.example.FTPClient.interfaces;

/**
 * Author: Sergey Shuruta
 * Date: 10/17/13
 * Time: 12:37 PM
 */
public interface FTPEventSingle {
    public void onEvent(int value);
}
