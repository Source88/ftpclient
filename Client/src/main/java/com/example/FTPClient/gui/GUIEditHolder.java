package com.example.FTPClient.gui;

import android.app.Activity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import com.example.FTPClient.R;

/**
 * Created with IntelliJ IDEA.
 * User: Source
 * Date: 13.10.13
 * Time: 15:42
 * To change this template use File | Settings | File Templates.
 */
public class GUIEditHolder {

    private Activity mActivity;

    public EditText name;
    public EditText server;
    public EditText port;
    public EditText login;
    public EditText password;
    public Button btnSave;

    public static final int ID_SAVE_CONNECTION = R.id.btn_save_connection;

    public GUIEditHolder(Activity activity, int layout) {
        mActivity = activity;
        mActivity.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        mActivity.setContentView(layout);
        initGUI();
    }

    private void initGUI() {
        name = (EditText) mActivity.findViewById(R.id.field_name);
        server = (EditText) mActivity.findViewById(R.id.field_server);
        port = (EditText) mActivity.findViewById(R.id.field_port);
        login = (EditText) mActivity.findViewById(R.id.field_login);
        password = (EditText) mActivity.findViewById(R.id.field_password);
        btnSave = (Button) mActivity.findViewById(R.id.btn_save_connection);
        btnSave.setOnClickListener((View.OnClickListener) mActivity);
    }
}
