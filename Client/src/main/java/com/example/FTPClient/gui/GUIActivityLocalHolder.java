package com.example.FTPClient.gui;

import android.app.Activity;
import android.widget.Button;
import com.example.FTPClient.R;


/**
 * Author: Sergey Shuruta
 * Date: 10/17/13
 * Time: 3:51 PM
 */
public class GUIActivityLocalHolder {

    private Activity mActivity;
    public Button selectBtn;
    public Button cancelBtn;

    public GUIActivityLocalHolder(Activity activity, int layout) {
        mActivity = activity;
        mActivity.setContentView(layout);
        initGUI();
    }

    private void initGUI() {
        selectBtn = (Button) mActivity.findViewById(R.id.select);
        cancelBtn = (Button) mActivity.findViewById(R.id.cancel_btn);
    }
}
