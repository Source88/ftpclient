package com.example.FTPClient.gui;

import android.app.Activity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import com.example.FTPClient.R;

/**
 * Created with IntelliJ IDEA.
 * User: Source
 * Date: 13.10.13
 * Time: 15:28
 * To change this template use File | Settings | File Templates.
 */
public class GUIConDialogHolder {

    private Activity mActivity;
    public ListView consList;
    public ImageView btnClose;
    public Button btnNewCon;

    public static final int ID_NEW_CON = R.id.btn_new_con;
    public static final int ID_CLOSE = R.id.btn_close;

    public GUIConDialogHolder(Activity activity, int layout) {
        mActivity = activity;
        mActivity.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        mActivity.setContentView(layout);
        initGUI();
    }

    private void initGUI() {
        consList = (ListView) mActivity.findViewById(R.id.list);
        btnClose = (ImageView) mActivity.findViewById(R.id.btn_close);
        btnNewCon = (Button) mActivity.findViewById(R.id.btn_new_con);
        consList.setOnItemClickListener((AdapterView.OnItemClickListener) mActivity);
        btnClose.setOnClickListener((View.OnClickListener) mActivity);
        btnNewCon.setOnClickListener((View.OnClickListener) mActivity);
    }
}
