package com.example.FTPClient.gui;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import com.example.FTPClient.R;
import com.example.FTPClient.fragments.FTPListFragment;

import java.io.File;

/**
 * Author: Sergey Shuruta
 * Date: 10/17/13
 * Time: 3:51 PM
 */
public class GUIFTPListHolder {

    private View mView;
    public ListView ftpList;
    public TextView path;

    public GUIFTPListHolder(FTPListFragment fragment,LayoutInflater inflater) {
        mView = inflater.inflate(R.layout.fragment_file_list, null);
        initGUI();
        ftpList.setOnItemClickListener(fragment);
    }

    private void initGUI() {
        ftpList = (ListView) mView.findViewById(R.id.file_list);
        path = (TextView) mView.findViewById(R.id.path);
    }

    public void displayPath(String dir) {
        path.setText("ftp:" + ((dir.length() == 0) ? File.separator : dir));
    }

    public View getView() {
        return mView;
    }
}
