package com.example.FTPClient.services.tasks;

import android.content.Context;
import android.util.Log;
import com.example.FTPClient.Request;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.entity.Connection;
import com.example.FTPClient.events.MessageEvent;
import com.example.FTPClient.services.Task;
import de.greenrobot.event.EventBus;
import it.sauronsoftware.ftp4j.FTPClient;

import java.io.File;

/**
 * Author: Sergey Shuruta
 * Date: 10/14/13
 * Time: 1:11 PM
 */
public class ConnectionTask extends Task {

    private Connection mConnection;
    private FTPClient mSession;

    public static final String TAG = ConnectionTask.class.getSimpleName();

    public ConnectionTask(Context context, Connection connection) {
        super(context);
        mSession = FTPApplication.getInstance().getFTPManager().getSession();
        mConnection = connection;
    }

    @Override
    public void run() {

        EventBus.getDefault().post(new MessageEvent(Request.REQUEST_CONNECTION_START));
        if(mSession.isConnected()) {
            if(mSession.isAuthenticated()) {
                EventBus.getDefault().post(new MessageEvent(Request.REQUEST_AUTHORIZATION_OK));
            } else {
                EventBus.getDefault().post(new MessageEvent(Request.REQUEST_AUTHORIZATION_ERROR));
            }
        }
        try {
            mSession.connect(mConnection.getServer(), mConnection.getPort());
            mSession.login(mConnection.getLogin(), mConnection.getPassword());
            mSession.setType(FTPClient.TYPE_BINARY);
            // TODO Чё то не срабатывает(
            //mSession.setAutoNoopTimeout(30000);
            mSession.changeDirectory(File.separator);
            if(mSession.isConnected()) {
                EventBus.getDefault().post(new MessageEvent(Request.REQUEST_CONNECTION_OK));
                if(mSession.isAuthenticated()) {
                    EventBus.getDefault().post(new MessageEvent(Request.REQUEST_AUTHORIZATION_OK));
                } else {
                    EventBus.getDefault().post(new MessageEvent(Request.REQUEST_AUTHORIZATION_ERROR));
                }
            } else {
                EventBus.getDefault().post(new MessageEvent(Request.REQUEST_CONNECTION_ERROR));
            }
        } catch(Exception e) {
            EventBus.getDefault().post(new MessageEvent(Request.REQUEST_CONNECTION_ERROR));
            Log.d(TAG, "Error: could not connect to host " + mConnection.getServer() + " with exception: " + e.getMessage());
        }
        EventBus.getDefault().post(new MessageEvent(Request.REQUEST_CONNECTION_FINISH));
    }
}
