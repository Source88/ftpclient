package com.example.FTPClient.services.tasks;

import android.content.Context;
import android.util.Log;

import com.example.FTPClient.entity.FileUnit;
import com.example.FTPClient.Request;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.events.MessageEvent;
import com.example.FTPClient.services.Task;
import de.greenrobot.event.EventBus;
import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPFile;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Source
 * Date: 15.10.13
 * Time: 23:20
 * To change this template use File | Settings | File Templates.
 */
public class ReadListTask extends Task {

    private String mDirPatch;
    private FTPClient mSession;

    public ReadListTask(Context context, String dirPatch) {
        super(context);
        mSession = FTPApplication.getInstance().getFTPManager().getSession();
        mDirPatch = (dirPatch.length() == 0) ? File.separator : dirPatch;
    }

    @Override
    public void run() {
        EventBus.getDefault().post(new MessageEvent(Request.REQUEST_READ_LIST_START));
        try {
            FTPFile[] list = mSession.list(mDirPatch);
            List<FileUnit> ftpFiles = new ArrayList<FileUnit>();
            for(FTPFile file : list) {
                if(file.getName().equals(".") || file.getName().equals("..")) continue;
                ftpFiles.add(new FileUnit(file, mDirPatch));
            }
            Collections.sort(ftpFiles, new Comparator<FileUnit>() {
                @Override
                public int compare(final FileUnit object1, final FileUnit object2) {
                    if(object1.isDir() && object2.isFile()) return -1;
                    if(object1.isFile() && object2.isDir()) return 1;
                    return object1.getName().compareTo(object2.getName());
                }
            });
            FTPApplication.getInstance().getFTPManager().setListFiles(ftpFiles);
            EventBus.getDefault().post(new MessageEvent(Request.REQUEST_READ_LIST_OK));
        } catch(Exception e) {
            EventBus.getDefault().post(new MessageEvent(Request.REQUEST_READ_LIST_ERROR));
            e.printStackTrace();
        }
        EventBus.getDefault().post(new MessageEvent(Request.REQUEST_READ_LIST_FINISH));
        EventBus.getDefault().unregister(this);
    }
}
