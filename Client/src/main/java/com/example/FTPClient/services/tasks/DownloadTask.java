package com.example.FTPClient.services.tasks;

import android.content.Context;
import android.util.Log;

import com.example.FTPClient.Request;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.entity.FileUnit;
import com.example.FTPClient.entity.HeapFiles;
import com.example.FTPClient.events.MessageEvent;
import com.example.FTPClient.managers.FTPManager;
import com.example.FTPClient.services.Task;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;
import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPFile;

/**
 * Author: Sergey Shuruta
 * Date: 11/25/13
 * Time: 5:37 PM
 */
public class DownloadTask extends Task {

    private FTPClient mSession;
    private HeapFiles mHeapFiles;
    private EventBus mBus;
    private boolean isError = false;
    private FTPManager mFTPManager;

    public static final String TAG = DownloadTask.class.getSimpleName();

    public DownloadTask(Context context) {
        super(context);
        mFTPManager = FTPApplication.getInstance().getFTPManager();
        mSession = mFTPManager.getSession();
        mHeapFiles = mFTPManager.getDownloadNode();
        mBus = EventBus.getDefault();
    }

    @Override
    public void run() {
        if(mHeapFiles == null) return;
        mBus.post(new MessageEvent(Request.REQUEST_DOWNLOAD_POOL_START));
        for(FileUnit file : mHeapFiles.getFiles()) {
            file.setLocalDir(mHeapFiles.getDownloadTo());
        }
        downloadFiles(mHeapFiles.getFiles());
        if(!isError)
            mFTPManager.removeDownloadPool();
        //mBus.post(new MessageEvent(Request.REQUEST_DOWNLOAD_POOL_FINISH));
    }

    private void downloadFiles(List<FileUnit> files) {
        for(FileUnit file : files) {
            if(isError) return;
            Log.d(TAG, "Select: " + file.getName());
            if(file.isDir()) {
                String dir = file.getName();
                Log.d(TAG, "Generate: " + file.getLocalDir() + File.separator + dir);
                File localDir = new File(file.getLocalDir() + File.separator + dir);
                localDir.mkdir();
                try {
                    String readdir = File.separator + file.getFtpDir() + dir + File.separator;
                    Log.d(TAG, "readdir: " + readdir);
                    FTPFile[] list = mSession.list(File.separator + readdir);
                    Log.d(TAG, "list: " + list.length);
                    List<FileUnit> dirFiles = new ArrayList<FileUnit>();
                    for(FTPFile f : list) {
                        if(f.getName().equals(".") || f.getName().equals("..")) continue;
                        //Log.d(TAG, "File: " + f.getName());
                        FileUnit newNode = new FileUnit(f, readdir);
                        newNode.setFtpDir(file.getFtpDir() + File.separator + dir + File.separator);
                        newNode.setLocalDir(file.getLocalDir() + File.separator + dir);
                        dirFiles.add(newNode);
                    }
                    if(dirFiles.size() > 0)
                        downloadFiles(dirFiles);
                } catch (Exception e) {
                    Log.d(TAG, "Exception: " + e.getMessage() + "(" + file.getName() + ")");
                    //mBus.post(new MessageEvent(Request.REQUEST_DOWNLOAD_POOL_ERROR));
                    isError = true;
                    return;
                }
            } else if(file.isFile()) {
                if(mFTPManager.isDownloadedFile(file)) continue;
                try {
                    mHeapFiles.setDownloadingFile(file);
                    File localFile = new File(file.getLocalDir() + File.separator + file.getName());
                    Log.d("TEST", "Path to file: " + localFile.getPath());
                    //Log.d(TAG, "Is replace " + file.getName() + ": " + mHeapFiles.isReplaceFile(file.getLocalDir() + File.separator + file.getName()));
                    if(!localFile.exists()/* || mHeapFiles.isReplaceAll() || mHeapFiles.isReplaceFile(file.getLocalDir() + File.separator + file.getName())*/) {
                        // Start download: /argest.com.ua/www/wp-content/themes/twentythirteen/images/search-icon-2x.png
                        // To: /storage/emulated/0/Download/twentythirteen/images/search-icon-2x.png

                        Log.d(TAG, "Start download: " + File.separator + file.getFtpDir() + File.separator + file.getName());
                        Log.d(TAG, "To: " + file.getLocalDir() + "/" + file.getName());
                        mSession.download(File.separator + file.getFtpDir() + File.separator + file.getName(), new File(file.getLocalDir() + File.separator + file.getName()), mHeapFiles.getListener());
                        mFTPManager.addToDownloadedFileList(file);
                    } else {
                        Log.d(TAG, file.getName() + " is present");
                        //mBus.post(new MessageEvent(Request.REQUEST_DOWNLOAD_FILE_PRESENT));
                        isError = true;
                        return;
                    }
                } catch(Exception e) {
                    Log.d(TAG, "Download " + file.getName() + " is error with exception: " + e.getMessage());
                    //mBus.post(new MessageEvent(Request.REQUEST_DOWNLOAD_POOL_ERROR));
                    isError = true;
                    return;
                }
            }
        }
    }
}
