package com.example.FTPClient.services.tasks;

import android.content.Context;
import android.util.Log;

import com.example.FTPClient.entity.FileUnit;
import com.example.FTPClient.Request;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.events.MessageEvent;
import com.example.FTPClient.services.Task;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;
import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPException;
import it.sauronsoftware.ftp4j.FTPFile;
import it.sauronsoftware.ftp4j.FTPIllegalReplyException;

/**
 * Created by Source on 11.01.14.
 */
public class DeleteFilesTask extends Task {

    private List<FileUnit> mFiles;
    private FTPClient mSession;

    public static final String TAG = DeleteFilesTask.class.getSimpleName();

    public DeleteFilesTask(Context context, List<FileUnit> files) {
        super(context);
        mFiles = files;
        mSession = FTPApplication.getInstance().getFTPManager().getSession();
    }

    @Override
    public void run() {
        try {
            EventBus.getDefault().post(new MessageEvent(Request.REQUEST_REMOVE_FILES_START));
            deleteFiles(mFiles);
            EventBus.getDefault().post(new MessageEvent(Request.REQUEST_REMOVE_FILES_FINISH));
        } catch(Exception e) {
            EventBus.getDefault().post(new MessageEvent(Request.REQUEST_REMOVE_FILES_FILED));
            Log.d(TAG, e.getMessage());
        }
    }

    public void deleteFiles(List<FileUnit> files) throws FTPException, IOException, FTPIllegalReplyException {
        for(FileUnit file : files) {
            if(file.isDir()) {
                try {
                    FTPFile[] list = mSession.list(file.getFtpDir() + File.separator + file.getName() + File.separator);
                    List<FileUnit> dirFiles = new ArrayList<FileUnit>();
                    for(FTPFile f : list) {
                        if(f.getName().equals(".") || f.getName().equals("..")) continue;
                        FileUnit newFile = new FileUnit(f, file.getFtpDir() + File.separator + file.getName());
                        newFile.setLocalDir(file.getLocalDir() + File.separator + file.getName());
                        dirFiles.add(newFile);
                    }
                    if(dirFiles.size() > 0)
                        deleteFiles(dirFiles);
                    mSession.deleteDirectory(file.getFtpDir() + File.separator + file.getName() + File.separator);
                    Log.d("TEST", "Delete dir: " + file.getFtpDir().concat(File.separator).concat(file.getName()).concat(File.separator));
                } catch (Exception e) {
                    Log.d(TAG, "Exception: " + e.getMessage() + "(" + file.getName() + ")");
                    EventBus.getDefault().post(new MessageEvent(Request.REQUEST_REMOVE_FILES_FILED));
                    return;
                }
            } else {
                mSession.deleteFile(file.getFtpDir().concat("/").concat(file.getName()));
                Log.d("TEST", "Delete file: " + file.getFtpDir().concat("/").concat(file.getName()));
            }
        }

    }
}
