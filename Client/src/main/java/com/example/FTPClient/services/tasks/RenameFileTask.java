package com.example.FTPClient.services.tasks;

import android.content.Context;
import android.util.Log;
import com.example.FTPClient.entity.FileUnit;
import com.example.FTPClient.Request;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.events.MessageEvent;
import com.example.FTPClient.services.Task;
import de.greenrobot.event.EventBus;
import it.sauronsoftware.ftp4j.FTPClient;

import java.io.File;

/**
 * Created by Sergey Shuruta on 1/31/14 at 3:56 PM.
 */
public class RenameFileTask extends Task {

    private FileUnit mFile;
    private String mNewName;
    private EventBus mBus;
    private FTPClient mSession;

    public static final String TAG = RenameFileTask.class.getSimpleName();

    public RenameFileTask(Context context, FileUnit file, String newName) {
        super(context);
        mBus = EventBus.getDefault();
        mSession = FTPApplication.getInstance().getFTPManager().getSession();
        mFile = file;
        mNewName = newName;
    }

    @Override
    public void run() {
        try {
            mBus.post(new MessageEvent(Request.REQUEST_RENAME_FILE_START));
            mSession.rename(mFile.getFtpDir().concat(File.separator).concat(mFile.getName()),
                    mFile.getFtpDir().concat(File.separator).concat(mNewName));
            mBus.post(new MessageEvent(Request.REQUEST_RENAME_FILE_FINISH));
        } catch(Exception e) {
            mBus.post(new MessageEvent(Request.REQUEST_RENAME_FILE_FILED));
            Log.d(TAG, e.getMessage());
        }
    }
}
