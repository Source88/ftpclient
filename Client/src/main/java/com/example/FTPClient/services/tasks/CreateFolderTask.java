package com.example.FTPClient.services.tasks;

import android.content.Context;
import android.util.Log;

import com.example.FTPClient.Request;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.events.MessageEvent;
import com.example.FTPClient.services.Task;

import java.io.File;

import de.greenrobot.event.EventBus;
import it.sauronsoftware.ftp4j.FTPClient;

/**
 * Created by Source on 11.01.14.
 */
public class CreateFolderTask extends Task {

    private File mFolder;
    private FTPClient mSession;

    public static final String TAG = CreateFolderTask.class.getSimpleName();

    public CreateFolderTask(Context context, File folder) {
        super(context);
        mFolder = folder;
        mSession = FTPApplication.getInstance().getFTPManager().getSession();
    }

    @Override
    public void run() {
        try {
            mSession.createDirectory(mFolder.getPath());
            EventBus.getDefault().post(new MessageEvent(Request.REQUEST_FOLDER_CREATED_FINISH));
        } catch(Exception e) {
            EventBus.getDefault().post(new MessageEvent(Request.REQUEST_FOLDER_CREATED_FILED));
            Log.d(TAG, e.getMessage());
        }
    }
}
