package com.example.FTPClient.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.example.FTPClient.entity.FileUnit;
import com.example.FTPClient.entity.Connection;
import com.example.FTPClient.managers.FTPManager;
import com.example.FTPClient.services.tasks.ConnectionTask;
import com.example.FTPClient.services.tasks.CreateFolderTask;
import com.example.FTPClient.services.tasks.DeleteFilesTask;
import com.example.FTPClient.services.tasks.DisconnectionTask;
import com.example.FTPClient.services.tasks.DownloadTask;
import com.example.FTPClient.services.tasks.ReadListTask;
import com.example.FTPClient.services.tasks.RenameFileTask;

import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Author: Sergey Shuruta
 * Date: 10/14/13
 * Time: 3:01 PM
 */
public class BridgeService extends Service {

    FtpBridgeBinder binder = new FtpBridgeBinder();
    ExecutorService executorService;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        executorService = Executors.newFixedThreadPool(10);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public void connect(Connection connection) {
        executorService.execute(new ConnectionTask(this, connection));
    }

    public void disconnect(boolean param, FTPManager.Reconnect reconnectStatus) {
        executorService.execute(new DisconnectionTask(this, param, reconnectStatus));
    }

    public void readList(String dir) {
        executorService.execute(new ReadListTask(this, dir));
    }

    public void startDownload() {
        executorService.execute(new DownloadTask(this));
    }

    public void createFolder(File folder) {
        executorService.execute(new CreateFolderTask(this, folder));
    }

    public void deleteFiles(List<FileUnit> mSelectedFiles) {
        executorService.execute(new DeleteFilesTask(this, mSelectedFiles));
    }

    public void renameFile(FileUnit file, String newName) {
        executorService.execute(new RenameFileTask(this, file, newName));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        executorService.shutdown();
    }

    public class FtpBridgeBinder extends Binder {
        public BridgeService getService() {
            return BridgeService.this;
        }
    }
}