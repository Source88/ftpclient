package com.example.FTPClient.services.tasks;

import android.content.Context;
import android.util.Log;

import com.example.FTPClient.Request;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.events.MessageEvent;
import com.example.FTPClient.managers.FTPManager;
import com.example.FTPClient.services.Task;
import de.greenrobot.event.EventBus;
import it.sauronsoftware.ftp4j.FTPClient;

/**
 * Created with IntelliJ IDEA.
 * User: Source
 * Date: 18.10.13
 * Time: 0:41
 * To change this template use File | Settings | File Templates.
 */
public class DisconnectionTask extends Task {

    private FTPClient mSession;
    private boolean mParam;
    private FTPManager.Reconnect mReconnectStatus;
    private EventBus mBus;

    public static final String TAG = DisconnectionTask.class.getSimpleName();

    public DisconnectionTask(Context context, boolean param, FTPManager.Reconnect reconnectStatus) {
        super(context);
        mParam = param;
        mReconnectStatus = reconnectStatus;
        mSession = FTPApplication.getInstance().getFTPManager().getSession();
        mBus = EventBus.getDefault();
    }

    @Override
    public void run() {
        try {
            mSession.disconnect(mParam);
            MessageEvent event = new MessageEvent(Request.REQUEST_DISCONNECT_OK);
            event.setValue(mReconnectStatus);
            mBus.post(event);
            Log.d(TAG, "Disconnected...");
        } catch(Exception e) {
            mBus.post(new MessageEvent(Request.REQUEST_DISCONNECT_ERROR));
            Log.d(TAG, "Error disconnected with exception: " + e.getMessage());
        }
        mBus.unregister(this);
    }
}
