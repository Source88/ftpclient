package com.example.FTPClient.services;

import android.content.Context;
import android.content.Intent;
import com.example.FTPClient.Const;

/**
 * Author: Sergey Shuruta
 * Date: 10/17/13
 * Time: 3:04 PM
 */
public abstract class Task implements Runnable {

    private Context mContext;

    protected Task(Context context) {
        mContext = context;
    }

}
