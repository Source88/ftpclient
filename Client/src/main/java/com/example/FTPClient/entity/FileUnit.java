package com.example.FTPClient.entity;

import android.graphics.drawable.Drawable;

import com.example.FTPClient.R;
import com.example.FTPClient.Utils;
import com.example.FTPClient.applications.FTPApplication;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;

import it.sauronsoftware.ftp4j.FTPFile;

/**
 * Created by Sergey Shuruta on 5/5/14 at 4:12 PM.
 */
public class FileUnit implements Serializable {

    private String mName;
    private String mDate;
    private String mSize;
    private long mSizeByte;
    private boolean isDir;
    private boolean isHidden;
    private Type mType;
    private String mFtpDir;
    private String mLocalDir;
    private Integer mProgress;

    private static final String FORMAT_TIME = "dd/MM/yyyy HH:mm";
    private static final SimpleDateFormat mFormatTime = new SimpleDateFormat(FORMAT_TIME);


    public enum Type {
        FTP,
        LOCAL
    }

    public FileUnit() {}

    public FileUnit(File file) {
        mType = Type.LOCAL;
        mName = file.getName();
        mDate = mFormatTime.format(file.lastModified());
        mSize = null;
        isDir = file.isDirectory();
        isHidden = file.isHidden();
        mFtpDir = null;
        mLocalDir = file.getAbsolutePath();
    }

    public FileUnit(FTPFile file, String ftpDir) {
        mType = Type.FTP;
        mName = file.getName();
        mDate = mFormatTime.format(file.getModifiedDate());
        mSize = Utils.getFormatSize(file.getSize());
        mSizeByte = file.getSize();
        isDir = file.getType() == FTPFile.TYPE_DIRECTORY;
        isHidden = false;
        mFtpDir = ftpDir;
        mLocalDir = null;
    }

    public void setLocalDir(String localDir) {
        mLocalDir = localDir;
    }

    public String getLocalDir() {
        return mLocalDir;
    }

    public void setFtpDir(String ftpDir) {
        mFtpDir = ftpDir;
    }

    public String getFtpDir() {
        return mFtpDir;
    }

    public String getName() {
        return mName;
    }

    public String getDate() {
        return mDate;
    }

    public String getSize() {
        return isDir ? "" : mSize;
    }

    public long getSizeByte() {
        return mSizeByte;
    }

    public Drawable getIcon() {
        return FTPApplication.getInstance().getResources().getDrawable(isDir ? R.drawable.ic_folder : R.drawable.ic_file);
    }

    public boolean isHidden() {
        return isHidden;
    }

    public boolean isDir() {
        return isDir;
    }

    public boolean isFile() {
        return !isDir;
    }

    public Type getType() {
        return mType;
    }

}
