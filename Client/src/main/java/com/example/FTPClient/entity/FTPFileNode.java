package com.example.FTPClient.entity;

import android.util.Log;

import com.example.FTPClient.Request;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.events.MessageEvent;

import de.greenrobot.event.EventBus;
import it.sauronsoftware.ftp4j.FTPDataTransferListener;
import it.sauronsoftware.ftp4j.FTPFile;

import java.io.File;

/**
 * Author: Sergey Shuruta
 * Date: 11/26/13
 * Time: 2:50 PM
 */
public class FTPFileNode {

    private FTPFile file;
    private String ftpDir = new String();
    private String localDir = new String();
    private long progress = 0;
    private long progressBytes = 0;
    private int status = FILE_STATUS_NULL;

    public static final int FILE_STATUS_NULL = 0;
    public static final int FILE_STATUS_PROGRESS = 1;
    public static final int FILE_STATUS_FAILED = 2;
    public static final int FILE_STATUS_COMPLETED = 3;
    public static final int FILE_STATUS_ABORTED = 4;
    private int id;
    private EventBus mBus;

    private static final String TAG = FTPFileNode.class.getSimpleName();

    public FTPFileNode(FTPFile file) {
        this.file = file;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setFTPDir(String ftpDir) {
        this.ftpDir = ftpDir;
    }

    public String getFTPDir() {
        return this.ftpDir;
    }

    public void setLocalDir(String localDir) {
        this.localDir = localDir;
    }

    public String getLocalDir() {
        return this.localDir;
    }

    public void setProgressBytes(long progressBytes) {
        FTPApplication.getInstance().getFTPManager().getDownloadNode().addProgress(progressBytes);
        this.progressBytes += progressBytes;
    }

    public void setDownloadFailed(FileUnit file) {
        FTPApplication.getInstance().getFTPManager().getDownloadNode().setDownloadFailed(file);
    }

    public int getProgress() {
        return (int)(100*(double)progressBytes/(double)file.getSize());
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return this.status;
    }

    public String getName() {
        return file.getName();
    }

    public long getSize() {
        return file.getSize();
    }

    public String downloadFrom() {
        return getFTPDir() + File.separator + file.getName();
    }

    public File downloadTo() {
        return new File(getLocalDir() + File.separator + file.getName());
    }

    public FTPDataTransferListener getListener(final EventBus bus) {
        return new FTPDataTransferListener() {
            @Override
            public void started() {
                Log.d(TAG, "Start download: " + getName());
                bus.post(new MessageEvent(Request.REQUEST_DOWNLOAD_FILE_START));
            }

            @Override
            public void transferred(int length) {
                setProgressBytes(length);
                MessageEvent event = new MessageEvent(Request.REQUEST_DOWNLOAD_FILE_PROGRESS);
                event.setValue(getId());
                bus.post(event);
            }

            @Override
            public void completed() {
                Log.d(TAG, "Completed: " + getName());
                setStatus(FTPFileNode.FILE_STATUS_COMPLETED);
                MessageEvent event = new MessageEvent(Request.REQUEST_DOWNLOAD_FILE_COMPLETED);
                event.setValue(getId());
                bus.post(event);
            }

            @Override
            public void aborted() {
                Log.d(TAG, "Aborted: " + getName());
                setStatus(FTPFileNode.FILE_STATUS_ABORTED);
                MessageEvent event = new MessageEvent(Request.REQUEST_DOWNLOAD_FILE_ABORTED);
                event.setValue(getId());
                bus.post(event);
            }

            @Override
            public void failed() {
                Log.d(TAG, "Failed: " + getName());
                //setDownloadFailed(FTPFileNode.this);
                setStatus(FTPFileNode.FILE_STATUS_FAILED);
                MessageEvent event = new MessageEvent(Request.REQUEST_DOWNLOAD_FILE_FAILED);
                event.setValue(getId());
                bus.post(event);

            }
        };
    }

    public int getType() {
        return file.getType();
    }
}