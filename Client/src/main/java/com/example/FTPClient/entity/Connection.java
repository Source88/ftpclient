package com.example.FTPClient.entity;

import android.os.Environment;

import java.io.File;
import java.io.Serializable;

public class Connection implements Serializable {

    public static int ACTIVE_MODE = 0;
    public static int PASSIVE_MODE = 1;
    private long id = 0;
    private String name = new String();
    private String server = new String();
    private int port = 21;
    private int mode = PASSIVE_MODE;
    private String login = new String();
    private String password = new String();
    private String encoding = "utf8";
    private Boolean anonymously = true;
    private String currentDir = File.separator;
    private String localDir;

    public static final String CONNECTION_ID = "connection_id";

    private static final String LOG_TAG = Connection.class.getSimpleName();

    public Connection() {
        localDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath();
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getEncoding() {
        return encoding;
    }

    public String getLocalDir() {
        return localDir;
    }

    public File getLocalDirFile() {
        return new File(localDir);
    }

    public void setLocalDir(String path) {
        this.localDir = path;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public String getIdString() {
        return String.valueOf(id);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getServer() {
        return server;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setPort(String port) {
        this.port = Integer.valueOf(port);
    }

    public int getPort() {
        return port;
    }

    public String getPortString() {
        return String.valueOf(port);
    }

    public void setMode(int mode) {
        if(mode == ACTIVE_MODE || mode == PASSIVE_MODE)
            this.mode = mode;
    }

    public void setMode(String mode) {
        setMode(Integer.valueOf(mode));
    }

    public int getMode() {
        return mode;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setAnonymously(Boolean anonymously) {
        this.anonymously = anonymously;
    }

    public Boolean isAnonymously() {
        return anonymously;
    }

    public String getFTPDir() {
        return currentDir;
    }

    public void setCurrentDir(String path) {
        currentDir = path;
    }
}