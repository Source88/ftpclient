package com.example.FTPClient.events;

import com.example.FTPClient.applications.FTPApplication;

/**
 * Created by Sergey Shuruta on 6/16/14 at 3:57 PM.
 */
public class SelectListEvent {

    private FTPApplication.ListType mListType;

    private SelectListEvent() {}

    public SelectListEvent(FTPApplication.ListType mListType) {
        this.mListType = mListType;
    }

    public FTPApplication.ListType getListType() {
        return mListType;
    }
}
