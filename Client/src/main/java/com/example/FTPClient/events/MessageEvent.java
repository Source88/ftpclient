package com.example.FTPClient.events;

import com.example.FTPClient.Request;

/**
 * Author: Sergey Shuruta
 * Date: 12/18/13
 * Time: 12:03 PM
 */
public class MessageEvent {

    private Request mAction;
    private Object mValue;

    public MessageEvent() {}

    public MessageEvent(Request action) {
        mAction = action;
    }

    public void setAction(Request action) {
        mAction = action;
    }

    public Request getAction() {
        return mAction;
    }

    public void setValue(Object value) {
        mValue = value;
    }

    public Object getValue() {
        return mValue;
    }
}
