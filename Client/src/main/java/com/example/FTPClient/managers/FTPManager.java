package com.example.FTPClient.managers;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.util.Log;

import com.example.FTPClient.Settings;
import com.example.FTPClient.applications.FTPApplication;
import com.example.FTPClient.entity.Connection;
import com.example.FTPClient.entity.FileUnit;
import com.example.FTPClient.entity.HeapFiles;
import com.example.FTPClient.services.BridgeService;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.connectors.FTPProxyConnector;

/**
 * Author: Sergey Shuruta
 * Date: 10/17/13
 * Time: 11:25 AM
 */
public class FTPManager {

    public boolean createFolder(File folder) {
        if(!isBoundToService) return false;
        mBridgeService.createFolder(folder);
        return true;
    }

    public void deleteFiles(List<FileUnit> selectedFiles) {
        if(!isBoundToService) return;
        mBridgeService.deleteFiles(selectedFiles);
    }

    public void readList() {
        if(!isBoundToService) return;
        mBridgeService.readList(getFTPCurrentDir());
    }

    public void renameFile(FileUnit file, String newName) {
        if(!isBoundToService) return;
        mBridgeService.renameFile(file, newName);
    }


    public void addToDownload(HeapFiles heapFiles) {
        mHeapFiles = heapFiles;
    }

    public void clearDownload() {
        mHeapFiles = null;
    }

    public void startDownload() {
        if(!isBoundToService) return;
        if(mHeapFiles == null) return;
        mBridgeService.startDownload();
    }











    private Context mContext;
    private Intent mServiceIntent;
    private Connection mConnection;
    private BridgeService mBridgeService;
    private FTPClient mSession;
    private List<String> currentPath = new ArrayList<String>();
    private List<FileUnit> mListFiles = new ArrayList<FileUnit>();
    private List<FileUnit> mDownloadedFileList = new ArrayList<FileUnit>();
    private HeapFiles mHeapFiles;
    private boolean isBoundToService = false;

    public static final String TAG = FTPManager.class.getSimpleName();
    private boolean mInternetStatus;




    public enum Reconnect {
        NO,
        YES,
        NO_AND_CLOSE;
    }

    public FTPManager(Context context) {
        mContext = context;
        mServiceIntent = new Intent(mContext, BridgeService.class);
        mSession = new FTPClient();
        if(Settings.getInstance().isUseProxy()) {
            FTPProxyConnector connector;
            if(Settings.getInstance().isUseProxyAuth()) {
                connector = new FTPProxyConnector(
                        Settings.getInstance().getProxyHost(),
                        Settings.getInstance().getProxyPort(),
                        Settings.getInstance().getProxyLogin(),
                        Settings.getInstance().getProxyPassword());
            } else {
                connector = new FTPProxyConnector(
                        Settings.getInstance().getProxyHost(),
                        Settings.getInstance().getProxyPort());
            }
            mSession.setConnector(connector);
        }
        mContext.bindService(mServiceIntent, new ServiceConnectionListener(), mContext.BIND_AUTO_CREATE);
        updateConnectivityStatus();
/*        mSession.addCommunicationListener(new FTPCommunicationListener() {
            @Override
            public void sent(String s) {
                Log.d(TAG, "Sent: " + s);
            }

            @Override
            public void received(String s) {
                Log.d(TAG, "Received: " + s);
            }
        });*/

    }

    private class ServiceConnectionListener implements ServiceConnection {

        public void onServiceConnected(ComponentName name, IBinder binder) {
            mBridgeService = ((BridgeService.FtpBridgeBinder) binder).getService();
            isBoundToService = true;
        }

        public void onServiceDisconnected(ComponentName name) {
            isBoundToService = false;
        }
    };

    public boolean updateConnectivityStatus() {
        mInternetStatus = false;
        ConnectivityManager cm = (ConnectivityManager) FTPApplication.getInstance().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                mInternetStatus = true;
            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                mInternetStatus = true;
        }
        return mInternetStatus;
    }

    public boolean getInternetStatus() {
        return mInternetStatus;
    }

    public FTPClient getSession() {
        return mSession;
    }

    public Connection getConnection() {
        return mConnection;
    }

    public void connect() {
        connect(getConnection());
    }

    public void connect(Connection connection) {
        mConnection = connection;
        if(!isBoundToService) return;
        mBridgeService.connect(mConnection);
    }

    public void disconnect(boolean param) {
        disconnect(param, Reconnect.NO_AND_CLOSE);
    }

    public void disconnect(boolean param, Reconnect reconnect) {
        if(!isBoundToService) return;
        mBridgeService.disconnect(param, reconnect);
    }

    public void reconnect() {
        disconnect(false, Reconnect.YES);
    }


    public void setListFiles(List<FileUnit> listFiles) {
        mListFiles = listFiles;
    }

    public List<FileUnit> getListFiles() {
/*        if(!Utils.isLandscapeOrientation()) {
            int index = 0;
            for(FTPFile file : new ArrayList<FTPFile>(mListFiles)) {
                if(file.getName().equals("..")) mListFiles.remove(index);
                index++;
            }
        }*/
        return mListFiles;
    }

    public HeapFiles getDownloadNode() {
        return mHeapFiles;
    }


/*
    public void startDownloadTo(FragmentActivity activity, BunchFiles node) {
        startDownload(node);
        Intent intent = new Intent(activity, LocalActivity.class);
        activity.startActivity(intent);
    }
*/

/*    public void startDownload(HeapFiles node, String patch) {
        node.setDownloadTo(patch);
        startDownload(node);
        startDownload();
    }*/

/*
    public void startDownload(BunchFiles node, String patch) {
        node.setDownloadTo(patch);
        startDownload(node);
        startDownload();
    }
*/

/*    public void startDownload(String patch) {
        HeapFiles node = getDownloadNode();
        node.setDownloadTo(patch);
        startDownload(node);
        startDownload();
    }*/

/*    public void startDownload(BunchFiles node) {
        startDownload(node, getConnection().getLocalDir());
    }*/

/*    public void startDownload() {
        if(!isBoundToService) return;
        if(mHeapFiles == null) return;
        mBridgeService.startDownload();
    }*/

    public String getFTPCurrentDir() {
        String path = "";
        for (String dir : currentPath) {
            path += File.separator + dir;
        }
        return path;
    }

    public void setCurrentDir(String path) {
        currentPath.add(path);
    }

    public boolean backCurrentDir() {
        if(currentPath.size() == 0) return false;
        currentPath.remove(currentPath.size() - 1);
        return true;
    }

    public void addToDownloadedFileList(FileUnit file) {
        mDownloadedFileList.add(file);
    }

    public boolean isDownloadedFile(FileUnit file) {
        return mDownloadedFileList.contains(file);
    }

    public void removeDownloadPool() {
        mDownloadedFileList.clear();
        mHeapFiles = null;
    }

/*    public boolean createFTPFolder(String name) {
        if(!isBoundToService) return false;
        mBridgeService.createFolder(name);
        return true;
    }*/





/*
    public void reConnection(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setValue(R.string.connection_error)
                .setPositiveButton(R.string.reconnect, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(isFtpConnected()) {
                            disconnect(false, true);
                        } else {
                            connect();
                        }
                        FTPApplication.getInstance().showProgressDialog(context, "Reconnection...", "Reconnection...");
                    }
                })
                .setNegativeButton(R.string.close_connection, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        disconnect(false);
                    }
                });
        builder.show();
    }
*/
}
